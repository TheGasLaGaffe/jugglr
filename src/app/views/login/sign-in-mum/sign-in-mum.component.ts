import { Component, OnInit } from '@angular/core';
import { JugglrServices } from '../../../Services/JugglrServices';
import { User } from '../../../models/user';
import { Router } from "@angular/router";
import { Observable, of } from 'rxjs';
import { debounceTime, switchMap, tap, map,distinctUntilChanged, catchError } from 'rxjs/operators';
import { AuthService, FacebookLoginProvider } from 'angular-6-social-login';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

declare var places: any;

@Component({
   selector: 'app-sign-in-mum',
   templateUrl: './sign-in-mum.component.html',
   styleUrls: ['./sign-in-mum.component.css']
})

export class SignInMumComponent implements OnInit {
   isRegistred: boolean = true;
   placesAutocomplete: any;
   fullname = [];
   message: string;
   registerStep = 0;
   regData: any;
   serviceNeeded: any[];
   serviceOffered: any[];
   
   postcodeSearch: any;
   searching = false;
   searchFailed = false;

   constructor(private socialAuthService: AuthService,
      private jservices: JugglrServices,
      private router: Router,
      private user: User,
      private modalService: NgbModal
   ) {
      //this.showRegistrationForm();
   }

   
   socialSignIn(socialPlatform: string) {

      let socialPlatformProvider;
      if (socialPlatform == "facebook") {
         socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
      }

      this.socialAuthService.signIn(socialPlatformProvider).then(
         (userData) => {
            //console.log(socialPlatform + " sign in data : ", userData);
            this.fullname = userData['name'].split(" ");

            //Save data user 
            this.user.facebook_id = userData['id'];
            this.user.email = userData['email'];
            this.user.facebook_token = userData['token'];
            this.user.firstname = this.fullname[0];
            this.user.lastname = this.fullname[1];
            this.user.profile_image_thumb = userData['image'];

            this.jservices.registration(this.user)
               .subscribe(data => {
                  //console.log(data)
                  localStorage.setItem('is_registered', data["data"]["is_registered"]);
                  //console.log(data["data"]["is_registered"]);
                  if (data["status"] == 1) {
                     if (data["data"]["is_registered"] == 1) {
                        localStorage.setItem('currentUser', JSON.stringify(data["data"]["result"]));
                        this.router.navigate(["/dashbord"],{ queryParams: { tab: 'a1' } });
                     }
                     else if (data["data"]["is_registered"] == 0) {// user not yet registered need more info
                        this.showRegistrationForm();
                     }
                  } else
                     this.message = data['message'];
               });
         }
      );
   }

   loadRegStep(step) {
      const element = document.querySelector("body")
      if (element) element.scrollIntoView({ behavior: 'smooth', block: 'start' })

      if(step > 0 && step < 5) {
         this.registerStep = step;
      }
      /*
      if (step == 1) { 
         this.registerStep = 1;
      }
      else if (step == 2) { 
         this.registerStep = 2;
      }
      else if (step == 3) { 
         this.registerStep = 3;
      }
      else if (step == 4) { 
         this.registerStep = 4;
      }
      */
   }

   saveRegisterStep(step) {
      if (step == 1) {
         if(this.searchFailed) {
            return;
         }
         //Save Data of step 1
         this.regData = JSON.parse(JSON.stringify(this.user));
         this.loadRegStep(2)
         
      }
      else if (step == 2) {
         var isValid = this.serviceNeeded.filter(t=> t.subservice.filter(n=>n.selected && n.selected == true).length > 0).length > 0;
         //console.log("Step2 isValid = " + isValid);
         if(!isValid) {
            alert("Please select at least one service to proceed.");
            return;
         }

         for (let i = 0; i < this.serviceNeeded.length; i++) {
            const eleService = this.serviceNeeded[i];
            var arrData: number[] = [];

            for (let j = 0; j < eleService.subservice.length; j++) {
               const eleSubService = eleService.subservice[j];
               if (eleSubService.selected != undefined && eleSubService.selected == true) {
                  arrData.push(eleSubService.subservice_id);
               }
            }
            if (arrData.length > 0) {
               //var data: any = {};
               //data.service_id =eleService.service_id
               //data.subservice = arrData;
               //this.regData.service_offered.push(arrData);

               var data: any = {};
               data[eleService.service_id] = arrData.join(',');
               this.regData.service_needed.push(data);
            }
         }
         this.loadRegStep(3)
         //console.log(this.regData);
      }
      else if (step == 3) {
         var isValid = this.serviceOffered.filter(t=> t.subservice.filter(n=>n.selected && n.selected == true).length > 0).length > 0;
         //console.log("Step3 isValid = " + isValid);
         if(!isValid) {
            alert("Please select at least one service to proceed.");
            return;
         }
         for (let i = 0; i < this.serviceOffered.length; i++) {
            const eleService = this.serviceOffered[i];
            var arrData: number[] = [];

            for (let j = 0; j < eleService.subservice.length; j++) {
               const eleSubService = eleService.subservice[j];
               if (eleSubService.selected != undefined && eleSubService.selected == true) {
                  arrData.push(eleSubService.subservice_id);
               }
            }

            if (arrData.length > 0) {
               var data: any = {};
               data[eleService.service_id] = arrData.join(',');
               this.regData.service_offered.push(data);
            }
         }
         //console.log(this.regData);
         this.jservices.registrationwithdetail(this.regData)
            .subscribe(data => {
               //console.log("Response= ", data);
               localStorage.setItem('is_registered', data["data"]["is_registered"]);
               //console.log(data["data"]["is_registered"]);
               if (data["status"] == 1) {
                  if (data["data"]["is_registered"] == 1) {
                     localStorage.setItem('currentUser', JSON.stringify(data["data"]["result"]));
                  }
                  //else if (data["data"]["is_registered"] == 0) {// user not yet registered need more info
                  //   this.showRegistrationForm();
                  //}
               } 
               else {
                  this.message = data['message'];
               }
               this.loadRegStep(4)
         });
      }
      else if (step == 4) {
         //this.router.navigate(["/dashbord"],{ queryParams: { tab: 'a1' } });
         this.router.navigate(["/dashbord"]);
      }
      //console.log("Saved Data = " , this.regData);
   }
   
   showRegistrationForm() {
      this.regData = this.user;
      this.regData.service_needed = [];
      this.regData.service_offered = [];
      this.registerStep = 1;
      this.jservices.getAllServicesWithSubService().subscribe(data => {
         this.serviceNeeded = JSON.parse(JSON.stringify(data['data']['serviceDetails']));
         this.serviceOffered = JSON.parse(JSON.stringify(data['data']['serviceDetails']));

         this.serviceNeeded = this.serviceNeeded.sort((a, b) => {
            if (a.service_name < b.service_name) return -1;
            else if (a.service_name > b.service_name) return 1;
            else return 0;
         });
         var index = this.serviceNeeded.findIndex(t=> t.service_name && (t.service_name + "").toLowerCase() == "payment");
         if(index > 0) {
            this.array_move(this.serviceNeeded,index,0);
         }

         this.serviceOffered = this.serviceOffered.sort((a, b) => {
            if (a.service_name < b.service_name) return -1;
            else if (a.service_name > b.service_name) return 1;
            else return 0;
         });
         index = this.serviceOffered.findIndex(t=> t.service_name && (t.service_name + "").toLowerCase() == "payment");
         if(index > 0) {
            this.array_move(this.serviceOffered,index,0);
         }
         //console.log("this.serviceNeeded = " , this.serviceNeeded);
         //console.log("Service", data);
      });
      this.goSignUp();
   }


   selectSubServiceItem(ser) {
      if (ser.selected == undefined) {
         ser.selected = false;
      }
      ser.selected = !ser.selected;
   }
   goSignUp() {
      this.isRegistred = !this.isRegistred;
      //setTimeout(() => {
      //   this.enableplaceautocomplete()
      //} , 100);
      this.loadRegStep(1);
   }

   saveMumUser() {
      //console.log("Mum User: " + this.user);
      this.jservices.signupmum(this.user)
         .subscribe(data => {
            //console.log(data);
            if (data['status'] == 1) {//user signedin
               localStorage.setItem('currentUser', JSON.stringify(data['data']['result']));
               this.router.navigate(["/"]);
            }
            else {
               this.message = data['message'];
            }

         });
   }

   enableplaceautocomplete() {
      this.placesAutocomplete = places({
         container: window.document.getElementById('address-input'),
         countries: 'au'
      });
      this.placesAutocomplete.on('change', e => {
         this.user.city = e.suggestion.city;
         this.user.suburb = e.suggestion.suburb;
         this.user.state = e.suggestion.administrative;
         this.user.country = e.suggestion.country;
         this.user.address = e.suggestion.value;
         this.user.postcode = e.suggestion.postcode;
         this.user.latitude = e.suggestion.latlng.lat;
         this.user.longitude = e.suggestion.latlng.lng;
         //console.log(e.suggestion);
      });
   }

   ngOnInit() {
   }
   open(content) {
      this.modalService.open(content, { centered: true }).result.then((result) => {
      }, (reason) => {
      });
   }
   
   search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term =>
         this.jservices.getAusPostcodes(term)
            .pipe(map(response => {
               if(response["data"]["localities"]["locality"] instanceof Array) {
                  return response["data"]["localities"]["locality"];
               }
               else if(response["data"]["localities"]["locality"] != undefined) {
                  return [response["data"]["localities"]["locality"]];
               }
               return response["data"]["localities"]["locality"];
            }))
            .pipe( 
               tap(() => this.searchFailed = false),
               catchError(() => {
                  this.searchFailed = true;
                  return of([]);
               })
            )
      ),
      tap(() => this.searching = false)
   )

   formatter = (x: { location: string, postcode:string }) =>  {  
      //console.log("x = ", x);
      return x.location + ""; 
   };
   inputformatter = (x: { location: string, postcode:string}) => {
      //x.search_value
      return x.postcode ? x.postcode + "" : "";
   };
   selectedItem(item){
      if(item) {
         //console.log("Selected Item", item.item);
         this.user.postcode = item.item.postcode;
         this.user.latitude = item.item.latitude;
         this.user.longitude = item.item.longitude;
         this.searchFailed = false;
         if(item.item.latitude == undefined || item.item.longitude == undefined) {
            this.jservices.getPlaceDetail(item.item.location).subscribe(data => {
               console.log(data);
               var res = data;
               if (res["status"] == 'ZERO_RESULTS' || res['error_message']) {
                 this.message = 'please enter a valide address';
                 this.searchFailed = true;
               }
               else if (data['results'].length > 0) {
                 this.user['latitude'] = data['results'][0].geometry["location"]["lat"];
                 this.user['longitude'] = data['results'][0].geometry["location"]["lng"];
                 console.log('after google location')
                 console.log(this.user);
               }
               else {
                  this.searchFailed = true;
               }
             });
         }
         /*
         this.searchVal = item.item.search_value;

         this.searchbox = item.item;
         this.changesearch()
         */
      }
      else {
         this.searchFailed = true;
      }
   }

   validatePostcode() {
      if(typeof this.postcodeSearch == "string") {
         this.searchFailed = true;
         this.postcodeSearch = "";
      }
   }
   array_move(arr:any[], old_index:number, new_index:number) {
      while (old_index < 0) {
         old_index += arr.length;
      }
      while (new_index < 0) {
         new_index += arr.length;
      }
      if (new_index >= arr.length) {
         var k = new_index - arr.length;
         while ((k--) + 1) {
            arr.push(undefined);
         }
      }
      arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);  
      return arr;
  };
}
