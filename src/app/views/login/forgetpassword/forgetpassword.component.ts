import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JugglrServices } from '../../../Services/JugglrServices';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {
  email:string;
  loading=false;
  resetdone=false;
  message="";
  constructor(private modal:NgbModal,private jservices: JugglrServices, private router:Router) { }

  ngOnInit() {
    this.email="";
  }
  opendsignup(){
    this.router.navigate(["signinbusiness"]);
  }
  forgetpassword(){
    this.message="";
    this.loading=true;
    this.jservices.resetpassword(this.email)
      .subscribe(data => {
        console.log(data);
        if (data['status'] == 1) {//user signedin
          
          this.loading=false;
          this.email="";
          this.resetdone=true;
        }
        else{
          this.message = data['message'];
          this.loading=false;
        }
      });
  }
}
