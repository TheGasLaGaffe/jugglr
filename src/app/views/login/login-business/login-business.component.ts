import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../../models/user';
import { JugglrServices } from '../../../Services/JugglrServices';
import { Job } from '../../../models/job';
import { Router } from '@angular/router';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlacesDirective } from 'src/app/google-places.directive';
import * as $ from 'jquery';

@Component({
  selector: 'app-login-business',
  templateUrl: './login-business.component.html',
  styleUrls: ['./login-business.component.css']
})
export class LoginBusinessComponent implements OnInit {
  login: boolean = true;
  loading = false;
  displayJobInfo = false;
  message: string;
  options = {
    types: [],
    componentRestrictions: { country: 'AU' }
  }
  constructor(private user: User, private jservices: JugglrServices, private job: Job, private router: Router) { }

  @ViewChild("placesRef") placesRef: GooglePlacesDirective;

  public handleAddressChange(address: Address) {
    this.user.address = address['formatted_address'];
    console.log(address);
  }

  ngOnInit() {
    const profile = JSON.parse(localStorage.getItem('currentUser'));
    if (profile != null) { 
        this.displayJobInfo = true;
        $('#loginForm').css('display', 'none');
    }
  }

  logif() {
    this.login = false;
  }

  registration() {
    this.loading = true;
    this.jservices.businessregistration(this.user)
      .subscribe(data => {
        console.log(data);
        if (data['status'] == 1) {//user signedin
          localStorage.setItem('currentUser', JSON.stringify(data['data']['result']));
          // this.jservices.addJob(this.job)
          //  .subscribe(data => {
          //    console.log(data);
          //    if (data['status'] == 1) {
          //      this.router.navigate(['congratulation']);
          //    } else {
          //      this.message = '' + data['message'];
          //    }
          //  });
          this.router.navigate(["/"]);
        }
        else {
          this.message = '' + data['message'];
        }
        this.loading = false;

      });
  }

  signin() {
    this.loading = true;
    //this.displayJobInfo = true;
    
    //$('#loginForm').css('display','none');
    

    this.jservices.businessLogin(this.user)
      .subscribe(data => {
        console.log(data);
        if (data['status'] == 1) {//user signedin
          localStorage.setItem('currentUser', JSON.stringify(data['data']['result']));
          this.displayJobInfo = true;
          $('#loginForm').css('display', 'none');
         
          //this.jservices.addJob(this.job)
          //  .subscribe(data => {
          //    console.log(data);
          //    if (data['status'] != null) {
          //      // this.router.navigate(['congratulation']);
          //      this.displayJobInfo = true;
          //      $('#loginForm').css('display', 'none');
          //    }
          //  });
        }
        else {
          this.message = data['message'];
        }
        this.loading = false;
      });

  }
  showlogin() {
    this.login = true;
  }

  startregistred() {
    this.loading = true;

    console.log(this.user.address);
    this.jservices.getPlaceDetail(this.user.address)
      .subscribe(data => {
        console.log(data);
        var res = data;
        if (res["status"] == 'ZERO_RESULTS' || res['error_message']) {
          this.message = 'please enter a valide address';
          this.loading = false;
          return false;
        }
        else if (data['results'].length > 0) {
          var componentslist = (data['results'][0]['address_components'])
          for (var i = 0; i < componentslist.length; i++) {
            if (componentslist[i].types[0] == 'postal_code')
              this.user['postcode'] = componentslist[i].short_name

            if (componentslist[i].types[0] == 'locality')
              this.user['suburb'] = componentslist[i].short_name

            if (componentslist[i].types[0] == 'administrative_area_level_1')
              this.user['state'] = componentslist[i].short_name

            if (componentslist[i].types[0] == 'country')
              this.user['country'] = componentslist[i].short_name

            if (componentslist[i].types[0] == 'administrative_area_level_2')
              this.user['city'] = componentslist[i].short_name
          }
          this.user['latitude'] = data['results'][0].geometry["location"]["lat"];
          this.user['longitude'] = data['results'][0].geometry["location"]["lng"];
          console.log('after google location')
          console.log(this.user);

          //registration
          this.registration();

        }
        else {
          this.message = 'Something went wrong!';
          this.loading = false;
        }

      });
  }

}
