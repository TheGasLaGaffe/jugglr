import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
@Component({
  selector: 'app-headline',
  templateUrl: './headline.component.html',
  styleUrls: ['./headline.component.css']
})
export class HeadlineComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  addjobMum(){
    localStorage.setItem('typeUser','1');
    this.router.navigate(['signinmum']);
  }

  addjobBusiness(){
    localStorage.setItem('typeUser','2');
    this.router.navigate(['signinbusiness']);
  }
   

}
