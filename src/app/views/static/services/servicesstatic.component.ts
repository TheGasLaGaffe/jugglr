import { Component, OnInit } from '@angular/core';
import {ModalConfigComponent} from '../../../utils/modal-config/modal-config.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

declare var $: any;
@Component({
  selector: 'app-servicesstatic',
  templateUrl: './servicesstatic.component.html',
  styleUrls: ['./servicesstatic.component.css']
})
export class ServicesstaticComponent implements OnInit {

  constructor(private modal:NgbModal) { }

  ngOnInit() {
    $('html,body').animate({scrollTop : 0} , 'slow');
  }
  signin(){
    localStorage.removeItem('from');
    this.modal.open(ModalConfigComponent, { centered: true });
  }
}
