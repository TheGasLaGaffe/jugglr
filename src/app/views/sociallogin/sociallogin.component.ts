import { Component, OnInit } from '@angular/core';
import { JugglrServices } from '../../Services/JugglrServices';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Job } from '../../models/job';
declare var places: any;

import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedinLoginProvider
} from 'angular-6-social-login';
import {User} from '../../models/user';

@Component({
  selector: 'app-sociallogin',
  templateUrl: './sociallogin.component.html',
  styleUrls: ['./sociallogin.component.css'],
  providers: [JugglrServices, NgbModalConfig, NgbModal]
})
export class SocialloginComponent implements OnInit {
  needmoredetails:boolean = false;
  placesAutocomplete: any;
  constructor(private socialAuthService: AuthService,
    private jservices: JugglrServices,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private job: Job,
              private currentuser:User
   ) { }

  fullname = [];
  to = '';
  from = '';
  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }
    // }else if(socialPlatform == "google"){
    //   socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    // } else if (socialPlatform == "linkedin") {
    //   socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    // }


    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + ' sign in data : ', userData);
        this.fullname = userData['name'].split(' ');
        this.currentuser.facebook_id=userData['id'];
        this.currentuser.facebook_token=userData['token'];
        this.currentuser.firstname=this.fullname[0];
        this.currentuser.lastname=this.fullname[1]
        this.currentuser.profile_image_thumb=userData['image'];
        this.currentuser.email=userData['email'];



        this.jservices.registration(this.currentuser)
          .subscribe(data => {
            console.log(data);
            localStorage.setItem('is_registered', data['data']['is_registered']);
            console.log(data['data']['is_registered']);
            if (data['status'] == 1) {
              if (data['data']['is_registered'] == 1) {//user signedin
                localStorage.setItem('currentUser', JSON.stringify(data['data']['result']));
                if (this.from == 'addjob') {
                  this.jservices.addJob(this.job)
                    .subscribe(data => {
                      console.log(data);
                      if (data['status'] != null) {
                        this.router.navigate(['congratulation']);
                      }
                    });
                } else {
                  this.router.navigate(['']);

                }

              } else if (data['data']['is_registered'] == 0) {// user not yet registered need more info
                //TODO:  create profile
                this.needmoredetails=true;
                setTimeout(() => {
                    this.getlocationdtail()
                  }
                  , 100);

              }
            }
          });
      }
    );
  }

  ngOnInit() {

    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        console.log(params)
        this.from = params['from'] || '';
      });
  }


  getlocationdtail() {
    if (this.needmoredetails) {
      this.placesAutocomplete = places({
        container: window.document.getElementById('address-input'),
        countries: 'au'
      });
      this.placesAutocomplete.on('change', e => {
        this.currentuser.city= e.suggestion.city;
        this.currentuser.suburb = e.suggestion.suburb;
        this.currentuser.state = e.suggestion.administrative;
        this.currentuser.country = e.suggestion.country;
        this.currentuser.address = e.suggestion.value;
        this.currentuser.postcode = e.suggestion.postcode;
        this.currentuser.latitude = e.suggestion.latlng.lat;
        this.currentuser.longitude = e.suggestion.latlng.lng;
        console.log(this.currentuser.postcode);
      });
    }

  }

  finishsignup(){
    this.jservices.registrationwithdetail(this.currentuser).subscribe(data => {
      console.log(data);
      if (data['data']['is_registered'] == 1) {//user signedin
        localStorage.setItem('currentUser', JSON.stringify(data['data']['result']));
        this.jservices.addJob(this.job)
          .subscribe(data => {
            console.log(data);
            if (data['status'] != null) {
              this.router.navigate(['congratulation']);
            }
          });
      }
        else{
          console.log('erreur')
      }


    });
    }

}
