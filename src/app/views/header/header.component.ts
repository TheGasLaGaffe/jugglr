import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfigComponent } from '../../utils/modal-config/modal-config.component';
import { CardModalComponent } from '../../utils/card-modal/card-modal.component';
import { Router, NavigationEnd } from '@angular/router';
import { JugglrServices } from '../../Services/JugglrServices';

declare var $: any;
@Component({
   selector: 'app-header',
   templateUrl: './header.component.html',
   styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
   headerBellCount = 0;
   isConnected: boolean = JSON.parse(localStorage.getItem('currentUser')) == null ? false : true;

   constructor(private jservices: JugglrServices, private modal: NgbModal, private router: Router) {
      this.router.events.subscribe((event) => {
         if (event instanceof NavigationEnd) {
            if (event.url === '/#howitwork' && $("#howitwork").length > 0) {
               $('html, body').animate({
                  scrollTop: ($("#howitwork").offset().top - 60)
               }, 1);
            }
         }
      });
   }

   ngOnInit() {
      this.navbarOpen = false;
      const profile = false;
      setInterval(() => {
         const profile = JSON.parse(localStorage.getItem('currentUser'));
         this.isConnected = profile == null ? false : true;
      }, 1000);

      var notifInterval = setInterval(() => {
         if (profile != null) {
            this.getnotifications();
            if (notifInterval) {
               clearInterval(notifInterval);
            }
         }  
         
      }, 1000);
   }
   
   logout() {
      this.navbarOpen = false;
      localStorage.clear();
      this.isConnected = false;
      this.router.navigate(['/']);
   }

   gotohome() {
      const profile = JSON.parse(localStorage.getItem('currentUser'));
      if (profile != null)
         this.router.navigate(["dashbord"]);
      else
         this.router.navigate(['/']);
   }

   signin() {
      this.navbarOpen = false;
      this.modal.open(ModalConfigComponent, { centered: true });
   }

   postjob() {
      this.navbarOpen = false;
      const profile = JSON.parse(localStorage.getItem('currentUser'));
      if (profile != null)
         this.router.navigate(['addjob']);
      else {
         localStorage.setItem('from', 'postjob');
         this.modal.open(ModalConfigComponent, { centered: true });
      }
   }

   navbarOpen = false;
   toggleNavbar() {
      this.navbarOpen = !this.navbarOpen;
   }
   browsjob() {
      this.navbarOpen = false;
      this.router.navigate(["jobs"]);
   }
   opendashboard() {
      this.navbarOpen = false;
      this.router.navigate(["dashbord"]);
   }
   howitwork() {
      this.navbarOpen = false;
      this.router.navigate(["howitwork"]);
   }

   
   getnotifications(){
      this.jservices.currentnotificationdata.subscribe(notificationdata => {
        this.headerBellCount = notificationdata['totalRecords'];
      });
    }
 
}
