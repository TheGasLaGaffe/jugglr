import { Component, OnInit } from '@angular/core';
import { JugglrServices } from '../../Services/JugglrServices';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
  providers:[JugglrServices,NgbCarouselConfig]
})
export class ServicesComponent implements OnInit {
  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  constructor(private servicesj: JugglrServices,config: NgbCarouselConfig) {
    config.interval = 300000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;

  }
  results = [];
  ngOnInit() {
    this.servicesj.getAllServices().subscribe(data => {
      data['data']['serviceDetails'].forEach(element => {
        //console.log(element['service_name']);

        this.results.push(element['service_name']);
      });
    });
  }

}
