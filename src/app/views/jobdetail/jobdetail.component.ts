import { Component, OnInit, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { JugglrServices } from '../../Services/JugglrServices';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BankAccountModalComponent } from '../../utils/bank-account-modal/bank-account-modal.component';
import { CardModalComponent } from '../../utils/card-modal/card-modal.component';
import { User } from '../../models/user';
import { AcceptjobModalComponent } from '../../utils/acceptjob-modal/acceptjob-modal.component';
import { ModalConfigComponent } from '../../utils/modal-config/modal-config.component';
import { PaymentServiceService } from 'src/app/payment-service.service';
import { environment } from 'src/environments/environment';

@Component({
   selector: 'app-jobdetail',
   templateUrl: './jobdetail.component.html',
   styleUrls: ['./jobdetail.component.css'],
   providers: [JugglrServices]
})
export class JobdetailComponent implements OnInit {
   message: string;
   a: AcceptjobModalComponent;
   sendingChatMessage:boolean = false;

   constructor(private paymentSvc: PaymentServiceService, private location: Location, private modal: NgbModal, private route: ActivatedRoute, private jservices: JugglrServices, private router: Router, private currentuser: User) {
   }

   job_id: string;
   job: any = [];
   comments: any = [];
   comment: string = '';
   profile: any;
   userid: string = '';
   usertoken: string = '';
   devicetype: string = '';
   noteapplication: string = '';
   dateapplication: string = '';
   canapply = false;
   handler: any;
   amount = 1000;
   statusName: string = '';
   backActiveTab: string = '';
   
   ngOnInit() {
      this.handler = StripeCheckout.configure({
         key: environment.stripeKey,
         image: '/assets/iconblue.png',
         locale: 'auto',
         token: token => {
            console.log(this.handler.stripeEmail);
            this.paymentSvc.processcard(token, this.amount)
            console.log(token)
            this.jservices.updateccdetails(token['id'], '', '', '')
               .subscribe(data => {
                  if (data['status'] == 1) {
                     var profile = JSON.parse(localStorage.getItem('currentUser'));
                     profile['stripe_cust_id'] = data['data']['stripe_cust_id'];
                     localStorage.setItem('currentUser', JSON.stringify(profile));
                     const profilex = JSON.parse(localStorage.getItem('currentUser'));
                     const userid = profilex['id'].toString();
                     const usertoken = profilex['user_token'];
                     const devicetype = profilex['device_type'];
                     this.currentuser.myconstructor(profilex);
                  }
               });
         },
         stripeEmail: email => { console.log(email) }
      });
      this.route
         .queryParams
         .subscribe(params => {
            // Defaults to 0 if no query param provided.
            this.job_id = params['id'].toString();
            this.backActiveTab = params['tab'].toString();
         });
      const profile = JSON.parse(localStorage.getItem('currentUser'));
      const userid = profile['id'].toString();
      const usertoken = profile['user_token'];
      const devicetype = profile['device_type'];
      this.currentuser.myconstructor(profile);
      this.jservices.getJobDetail(this.job_id, '0', '0', userid, usertoken, devicetype)
         .subscribe(data => {
            if (data['status'] == 1) {

               this.job = data['data'];
               if (this.currentuser.Id == this.job.user_id)
                  this.updatebool = true;

               if (this.job.status == 1) {
                  if (this.job.is_assigned == 0) {
                     this.statusName = "OPEN";
                  }
                  else {
                     this.statusName = "ASSIGNED";
                  }
               }
               console.log(this.job);
            }
            else {
               this.message = '' + data['message'];
            }
         });

      this.loadComments();
      console.log(this.currentuser)
   }
   
   npage = 1;
   nTotalCommentPage = 0;
   loadComments() {
      const profile = JSON.parse(localStorage.getItem('currentUser'));
      this.userid = profile['id'].toString();
      this.usertoken = profile['user_token'];
      this.devicetype = profile['device_type'];
      this.jservices.listJobComment(this.job_id, this.userid, this.usertoken, this.devicetype, this.npage)
         .subscribe(data => {
            console.log(data)
            if (data['status'] == 1) {
               data['data']['Comments'].forEach(element => {
                  this.comments.push(element)
               });
               this.nTotalCommentPage = data['data']['totalpages'];

               if (this.nTotalCommentPage > this.npage) {
                  this.npage++;
                  this.loadComments();
               }
            } else
               this.message = '' + data['message'];
         });
   }
   /*
   onMessageScroll() {
      console.log("Message Scrolled");
      if (this.nTotalCommentPage > this.npage) {
         this.npage++;
         this.loadComments();
      }
   }
   */

   addComment() {
      console.log(this.comment);
      this.sendingChatMessage = true;
      var lastComment = {
         comment: this.comment,
         datetime: new Date().getTime()/1000,
         user_id: this.userid
      }
      this.comments.push(lastComment);
      var txtComment = this.comment;
      this.comment = '';

      this.jservices.addJobComment(this.job_id, txtComment, this.userid, this.usertoken, this.devicetype)
         .subscribe(data => {
            console.log(data);
            if (data['status'] == 1) {
               this.comments = [];
               this.loadComments();
               this.comment = '';
            } else {
               this.message = '' + data['message'];
            }
            this.sendingChatMessage = false;
      });
   }

   parseChatMessage(message) {
      var webPatern = /(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/gim;
      var searchExp = new RegExp(webPatern)
      message = message.replace(searchExp, 
         str =>  "<a href='"+this.getHttpLink(str)+"' target='_blank' rel='noopener noreferrer' class='embedded-link'>" + str + "</a>");


      var emailPatern = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/gim
      searchExp = new RegExp(emailPatern)
      message = message.replace(searchExp, 
         str =>  "<a href='mailto:"+ str +"' rel='noopener noreferrer' class='embedded-emaillink'>" + str + "</a>");

      return message;
   }
   getHttpLink(link:string) {
      link = link.trim()
      if(link.length > 0 && link.indexOf("http://") == -1 && link.indexOf("https://") == -1) {
         link = "http://" + link;
      }
      return link;
   }


   goBack() {
      // this.location.back(); // <-- go back to previous location on cancel
      this.router.navigate(['/dashbord'], { queryParams: { b: this.job.type, tab: this.backActiveTab } });

   }

   timeSince(date) {
      return moment(date).fromNow();
   }

   goprofile(id) {
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: id } });
   }

   accceptJob() {
      if (this.job['type'] == 0) {
         if (this.currentuser.stripe_acct_id == '') {
            this.modal.open(BankAccountModalComponent);
         }
         else {
            this.canapply = true;
         }
      } else {
         if (this.currentuser.stripe_cust_id == '') {
            this.modal.open(CardModalComponent);

         }
         else {
            this.canapply = true;

         }
      }
   }

   accceptdone() {
      console.log(this.dateapplication);
      this.jservices.applytojob(this.job_id, this.noteapplication, this.dateapplication).subscribe(data => {
         if (data['status'] == 1) {
            this.router.navigate(['/dashbord']);
         }
      }
      );
   }
   updatebool = false;
   updateprocess = false;
   showupdate() {
      this.updateprocess = true;
   }
   updateGig() {
      //update
      this.jservices.postupdategig(this.job).subscribe(data => {

         this.updateprocess = false;
      });

   }
   accceptapplicant(x: any) {
      this.amount = this.job.amount * 100
      console.log(this.currentuser)
      if (this.currentuser.stripe_cust_id == '') {
         this.handlePayment()
      }
      else {
         console.log(this.currentuser)
         this.jservices.assignjob(x.offer_id, this.job.job_id, x.applicant_id, 'ASSIGN', this.job.type).subscribe(data => {
            if (data['status'] == 1) {
               location.reload();
            }
         })
      }
   }
   deneyapplicant(x: any) {
      this.jservices.assignjob(x.offer_id, this.job.job_id, x.applicant_id, 'UNASSIGN', this.job.type).subscribe(data => {
         if (data['status'] == 1) {
            location.reload();
         }
      })
   }


   handlePayment() {
      this.handler.open({
         name: 'Jugglr',
         excerpt: this.job.customservice,
         description: this.job.customservice,
         amount: this.amount,


      });
   }
   @HostListener('window:popstate')
   onPopstate() {
      this.handler.close()
   }



   someMethod() {

      //this.subscribehttp().subscribe(data => {});
   }


}
