import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { NgbModal, NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../models/user';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PaymentServiceService } from 'src/app/payment-service.service';

import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlacesDirective } from 'src/app/google-places.directive';
import { JugglrServices } from 'src/app/Services/JugglrServices';
@Component({
  selector: 'app-dashboard-menu',
  templateUrl: './dashboard-menu.component.html',
  styleUrls: ['./dashboard-menu.component.css']

})
export class DashboardMenuComponent implements OnInit {
  routelocation: string;
  premiumstate = false;
  handler: any;
  //amount = 1000;
  //amount = 5500; //$55
  amount = 0;
  productPlanID:string = '';
  pstr = "";
  user: User;
  notifications: any = [];
  notificationsCount: any = {"marketplace":0,"booking":0,"message":0,"notification":0};
  pagenum=1;
  modalReference:any;
  paymentSuccessRoute:string = '';

  constructor(private jservices: JugglrServices,private paymentSvc: PaymentServiceService, private http: HttpClient, config: NgbRatingConfig, public currentuser: User, private router: Router, private modalService: NgbModal, private db: AngularFireDatabase) {
    config.max = 5;
    config.readonly = true;
    router.events.subscribe((url: any) => {
      //console.log(url["url"])
      this.routelocation = url["url"];
    });
  }

  image: any;

  ngOnInit() {
    this.currentuser.myconstructor(JSON.parse(localStorage.getItem('currentUser')));
    this.user = this.currentuser;
    var ref = (this.db.object('/payments/premium/'));
    //console.log(ref)
    ref.valueChanges().subscribe(item => {
      this.pstr = item.toString();
      const premiums = item.toString().split(',')
      if (premiums.includes(this.currentuser.Id))
        this.premiumstate = true;
      else
        this.premiumstate = false;
    })
    this.handler = StripeCheckout.configure({

      key: environment.stripeKey,
      image: '/assets/iconblue1.png',
      locale: 'auto',
      token: token => {
         //console.log(this.handler.stripeEmail);
         //this.paymentSvc.processPayment_test(token, this.amount, this.productPlanID)
         var objthis = this;
         this.paymentSvc.processPayment(token, this.amount, this.productPlanID,function() {
            objthis.pstr = objthis.pstr + "," + objthis.currentuser.Id
            ref.set(objthis.pstr);
            if(objthis.modalReference) {
               objthis.modalReference.close();
               if(objthis.paymentSuccessRoute != "") {
                  //objthis.router.navigate(['/'+ objthis.paymentSuccessRoute]);
                  objthis.router.navigateByUrl('dashbord/(dashboardContent:'+objthis.paymentSuccessRoute+')');
               }
            }
         });
      },
      stripeEmail: email => { console.log(email) }
    });
    this.getallnotifications();
    this.listnotificationdetails();
    this.getProductPlanPrice();
  }

   getProductPlanPrice() {
      this.paymentSvc.getAllPlans().subscribe(item => {
         this.amount = 0;
         this.productPlanID = "";
         //console.log("plans",item);
         if(item && item["data"] && item["data"].length > 0) {
            var plans = item["data"].filter(t=>t.product == environment.stripeProductID && t.active == true);
            if(plans.length > 0) {
               this.amount = plans[0].amount;
               this.productPlanID = plans[0].id;
            }
         }
      });
   }

  addjobclick() {
    localStorage.setItem('typeUser', this.currentuser.type + '');
    this.router.navigate(['/addjob']);

  }

  open(content, route) {
    this.paymentSuccessRoute = route;
    this.modalReference = this.modalService.open(content, { centered: true });
    this.modalReference.result.then((result) => {
    }, (reason) => {
    });
  }


  handlePayment() {
     console.log("Hello ");
    this.handler.open({
      name: 'Jugglr Business Premium',
      excerpt: 'Deposit Funds to Account',
      description: '',
      amount: this.amount,
      interval: 'month',
      currency: 'aud'

    });
  }

  getallnotifications(){
    const profile = JSON.parse(localStorage.getItem('currentUser'));    
    this.jservices.getPendingNotification(profile['id'].toString(), profile['user_token'], profile['device_type'],this.pagenum)
    .subscribe(notificationdata => {
      if (notificationdata['status'] != null){        
        notificationdata['data']['notification'].forEach(element => {          
          this.notifications.push(element);
        });        
      }

      if(this.pagenum == notificationdata['data']['totalpages']) {
        let notificationfinaldatalist = {};
        notificationfinaldatalist['notification'] = this.notifications;
        notificationfinaldatalist['totalRecords'] = this.notifications.length;
        this.jservices.updatenotificationreaddata(notificationfinaldatalist);
      }
      if(this.pagenum < notificationdata['data']['totalpages']){
        this.pagenum++;
        this.getallnotifications();
      }
      
    });
  }
  listnotificationdetails(){
    this.jservices.currentnotificationdata.subscribe(notificationdata => {      
      if (notificationdata['totalRecords'] > 0){
        this.notificationsCount["marketplace"] = this.notificationsCount["booking"] = this.notificationsCount["message"] = this.notificationsCount["notification"] = 0;
        this.notificationsCount["notification"] = notificationdata['totalRecords'];
        for (var i = 0; i < notificationdata['notification'].length; i++) {
          if(notificationdata['notification'][i]['data']['redirect'] == "JOBDETAIL")
          {
            this.notificationsCount["booking"]++;
          }
          if(notificationdata['notification'][i]['data']['redirect'] == "OFFERSUMMARY" || notificationdata['notification'][i]['data']['redirect'] == "MYTODO" || notificationdata['notification'][i]['data']['redirect'] == "USERPROFILE")
          {
            this.notificationsCount["marketplace"]++;
          }
          if(notificationdata['notification'][i]['data']['redirect'] == "CHAT")
          {
            this.notificationsCount["message"]++;
          }
          //if(notificationdata['notification'][i]['data']['redirect'] == "USERPROFILE" || notificationdata['notification'][i]['data']['redirect'] == "VERIFIED" || notificationdata['notification'][i]['data']['redirect'] == "INVITEJUGGLR")
          //{
          //  this.notificationsCount["notification"]++;
          //}
        };
      }
    });
  }

  @HostListener('window:popstate')
  onPopstate() {
    this.handler.close()
  }
}
