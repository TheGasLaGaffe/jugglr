import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../../../models/user';
import { JugglrServices } from '../../../../Services/JugglrServices';
import {ActivatedRoute,Router} from '@angular/router';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlacesDirective } from 'src/app/google-places.directive';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/internal/operators/finalize';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {

  loading=false;
  login: boolean = true;
  message: string;
  options = {
    types: [],
    componentRestrictions: { country: 'AU' }
    }
  from="";
  constructor(private storage: AngularFireStorage,public user: User, private jservices: JugglrServices, private router:Router,private route: ActivatedRoute, ) {
   
    
   }

  ngOnInit() {
    this.user.myconstructor(JSON.parse(localStorage.getItem('currentUser')));
    
  }

  @ViewChild("placesRef") placesRef : GooglePlacesDirective;
    
        public handleAddressChange(address: Address) {
          this.user.address= address['formatted_address'];
        console.log(address);
    }
    loadinxg=false;
  registration(){
    console.log(this.user);
    this.jservices.updatebusinessuser(this.user)
      .subscribe(data => {
        console.log(data);
        if (data['status'] == 1) {//user signedin
          var x:JSON=JSON.parse(localStorage.getItem('currentUser'))
          this.user.toJSONdata(x)
          console.log(x)
       localStorage.setItem('currentUser',JSON.stringify(x))
          this.router.navigate(["/dashbord"]);    
            }
        else{
          this.message = data['message'];
        }
        this.loading=false;
      });
  }

  downloadURL: Observable<string>;
  uploadPercent: Observable<number>;

  uploadFile(event) {
    this.loadinxg=true;
    const file = event.target.files[0];
    const filePath = 'webimg/'+ Date.now()+file.name;
    console.log(filePath)
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();

    task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = fileRef.getDownloadURL() 
        fileRef.getDownloadURL().subscribe(res => this.user.profile_image_thumb = res)
      
      })
      )
  .subscribe()
  }

  startregistred(){
    this.user
    this.loading=true;
    this.jservices.getPlaceDetail(this.user.address)
      .subscribe(data => {
        var res=data;
        if(res["status"]=='ZERO_RESULTS' || res['error_message']){
            this.message='please enter a valide address';
            this.loading=false;
            return false;
        }
        else if(data['results'].length>0){
          var componentslist=(data['results'][0]['address_components'])
          for (var i=0;i< componentslist.length;i++){
              if(componentslist[i].types[0]=='postal_code')
              this.user['postcode']=componentslist[i].short_name
      
              if(componentslist[i].types[0]=='locality')
              this.user['suburb']=componentslist[i].short_name
      
              if(componentslist[i].types[0]=='administrative_area_level_1')
              this.user['state']=componentslist[i].short_name
      
              if(componentslist[i].types[0]=='country')
              this.user['country']=componentslist[i].short_name
      
              if(componentslist[i].types[0]=='administrative_area_level_2')
              this.user['city']=componentslist[i].short_name
          }
          this.user['latitude']=data['results'][0].geometry["location"]["lat"];
          this.user['longitude']=data['results'][0].geometry["location"]["lng"];
          console.log('after google location')
          console.log(this.user);
  
            //registration
            this.registration();
  
        }
        else{
          this.message='Something went wrong!';
          this.loading=false;
        }

      });
  }

}
