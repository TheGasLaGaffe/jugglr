import { Component, OnInit, HostListener } from '@angular/core';
import { JugglrServices } from '../../../../Services/JugglrServices';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { User } from '../../../../models/user';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { debounceTime, switchMap, tap } from 'rxjs/operators';

@Component({
   selector: 'app-userprofilelist',
   templateUrl: './userprofilelist.component.html',
   styleUrls: ['./userprofilelist.component.css']
})
export class UserprofilelistComponent implements OnInit {
   jobs = [];
   bpage: number = 1;
   message: string = '';
   page: number = 1;
   type: number = 1;

   pagenumber = 1;
   users: any;
   totalpages = 0;
   uprofessionals: any;
   ubusinesses: any;
   showtype: number;
   userId: string = '';
   lat: any;
   lng: any;
   blnSearched:boolean=false;
   constructor(private jservices: JugglrServices, private router: Router, private route: ActivatedRoute, private currentuser: User) { }

   ngOnInit() {
      var k = 1
      this.users = [];
      this.ubusinesses = [];
      this.uprofessionals = [];
      this.showtype = 1;
      this.getNetworkData();

   }

   getNetworkData() {
      this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.userId = params['id'].toString();
        this.lat = params['lat'].toString();
        this.lng = params['lng'].toString();
      });

      this.jservices.getlistviewdataforanalytics(this.lat, this.lng, this.userId,''+ this.pagenumber).subscribe(data => {
         this.totalpages = data['data']['totalpages']
         for (let i = 0; i < data['data']['users'].length; i++) {
            let tmpuserlists = data['data']['users'][i]['userData'];
            for (let k = 0; k < tmpuserlists.length; k++) {
               var usersp = tmpuserlists[k];
               if (usersp.type == 2) {
                  //this.ubusinesses.push(usersp);
                  this.uprofessionals.push(usersp); // Tempararly added this line, there is no html written for ubusinesses object so added this line here for quickfix.
               }
               else if (usersp.type == 1) {
                  this.uprofessionals.push(usersp);
               }
            }
         }
      });
   }
   getDocumentHeight() {
      var body = document.body,
         html = document.documentElement;

      return Math.max(body.scrollHeight, body.offsetHeight,
         html.clientHeight, html.scrollHeight, html.offsetHeight);
   }
   onPageScroll() {
      this.getnext();
   }

   getnext() {
      if (this.pagenumber < this.totalpages) {
         this.pagenumber++;

         this.jservices.getlistviewdataforanalytics(this.lat, this.lng, this.userId,''+ this.pagenumber).subscribe(data => {
            this.totalpages = data['data']['totalpages']
            for (let i = 0; i < data['data']['users'].length; i++) {
               let tmpuserlists = data['data']['users'][i]['userData'];
               for (let k = 0; k < tmpuserlists.length; k++) {
                  var usersp = tmpuserlists[k];
                  if (usersp.type == 2) {
                     this.ubusinesses.push(usersp);
                  }
                  else if (usersp.type == 1) {
                     this.uprofessionals.push(usersp);
                  }
               }
            }
         });
      }
   }

   shortlist(usr: any, i: number) {
      this.jservices.addusertoshortlist(usr.user_id)
         .subscribe(data => {
            if (this.users[i].is_shortlist)
               this.users[i].is_shortlist = 0;
            else
               this.users[i].is_shortlist = 1;
            //console.log(data['message']);
         });
   }
   goUserDetail(userId) {
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: userId } });
   }
}
