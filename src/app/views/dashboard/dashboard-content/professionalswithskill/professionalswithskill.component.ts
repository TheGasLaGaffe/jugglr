import { Component, OnInit } from '@angular/core';
import { JugglrServices } from '../../../../Services/JugglrServices';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {User} from '../../../../models/user';
import { Location } from '@angular/common';

@Component({
  selector: 'app-professionalswithskill',
  templateUrl: './professionalswithskill.component.html',
  styleUrls: ['./professionalswithskill.component.css']
})
export class ProfessionalswithskillComponent implements OnInit {
  jobs=[];
  bpage:number=1;
  message:string='';
  page:number=1;
  type:number=1;
  filter: any[] = [{id:'1',name:'all jobs'},{id:'2',name:'my jobs'},{id:'3',name:'saved jobs'},{id:'4',name:'assigned jobs'}];
  filtertext:string='1';
  textsearch:string;
  usersprofiles=[];
  businessarround=[];
  valuesearch=[{id:56,searchtext:"Sales & Marketing"},
  {id:43,searchtext:"ADMIN SUPPORT"},
  {id:55,searchtext:"Web design"},
  {id:69,searchtext:"Customer service"},
  {id:58,searchtext:"Photography"},
  {id:52,searchtext:"Website & app testing"},
  {id:54,searchtext:"Graphic design"},
  {id:49,searchtext:"Social media marketing"},
  {id:42,searchtext:"Bookkeeping & accounting"},
  {id:50,searchtext:"Content creation & comms"},
];
  listofids:any;
  pagegenumber=1;
  users:any;
  totalpages=0;
  searchsubserviceid:string;
  searchtext:string;
  constructor(private location: Location,private jservices: JugglrServices, private router:Router,private route:ActivatedRoute, private currentuser:User) { }

  ngOnInit() {
    var k=1
    this.users=[];
    this.route
    .queryParams
    .subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.searchsubserviceid = params['id'].toString();
      this.searchtext = params['searchtext'].toString();

    });
    this.jservices.getresultfromfire(this.searchsubserviceid+'').subscribe(data=>{
    this.listofids=data;
    this.jservices.getlistuserswithskill(''+this.listofids,this.searchtext,this.pagegenumber+"").subscribe(data=>{
      console.log(data)
      this.totalpages=data['data']['totalpages']
      for(let i=0;i<data['data']['users'].length;i++){
        for(let j=0;j<data['data']['users'][i]['userData'].length;j++){
          var usersp=data['data']['users'][i]['userData'][j];
          usersp["suburb"]=data['data']['users'][i]['suburb'];
          this.users.push(usersp);
        }
      }
      this.getnext()
    })
    })


    
  }
  getnext(){
    this.pagegenumber++;
    if (this.pagegenumber<this.totalpages){
      this.jservices.getlistuserswithskill(''+this.listofids,this.searchtext,this.pagegenumber+"").subscribe(data=>{
        console.log(data)
        this.totalpages=data['data']['totalpages']
        for(let i=0;i<data['data']['users'].length;i++){
          for(let j=0;j<data['data']['users'][i]['userData'].length;j++){
            var usersp=data['data']['users'][i]['userData'][j];
            usersp["suburb"]=data['data']['users'][i]['suburb'];
            this.users.push(usersp);
          }
        }
        this.getnext()
        console.log(this.users)

      })
    }
  }
  shortlist(usr:any,i:number){
    this.jservices.addusertoshortlist(usr.user_id)
      .subscribe(data => {
        if(this.users[i].is_shortlist)
        this.users[i].is_shortlist=0;
        else
        this.users[i].is_shortlist=1;
        console.log(data['message']);
      });
  }
  goUserDetail(userId){
    this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: userId } });
  }
  goBack() {
    this.location.back(); // <-- go back to previous location on cancel

    // this.router.navigate(['/dashbord']);
  }
}
