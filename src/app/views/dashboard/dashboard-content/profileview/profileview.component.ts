import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../../../models/user';
import { JugglrServices } from '../../../../Services/JugglrServices';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { debounceTime, switchMap, tap } from 'rxjs/operators';
import { Offer } from 'src/app/models/offer';

@Component({
  selector: 'app-profileview',
  templateUrl: './profileview.component.html',
  styleUrls: ['./profileview.component.css']
})
export class ProfileviewComponent implements OnInit {

  newoffer=new Offer();
  profileViewData: JSON;
  userId: string = '';
  isshortlist: number = 0;
  TypeUser: boolean;
  model: string = '';
  strService: string = '';
  placesAutocomplete: any;
  transaction: any = [];
  constructor(private location: Location, private currentuser: User, private modalService: NgbModal, private jservices: JugglrServices, private route: ActivatedRoute, private router: Router ) {
    this.profileViewData = JSON.parse("{}");
    const profile = JSON.parse(localStorage.getItem('currentUser'));
    this.currentuser.myconstructor(profile);
    this.transaction['user_id'] = this.currentuser.Id;
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.userId = params['id'].toString();
      });

  }

  ngOnInit() {
    this.jservices.getUserDetail(this.userId, this.currentuser)
      .subscribe(data => {
        console.log(data);
        this.profileViewData = data['data']['0'];
      });

  }


  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      tap(() => console.log(this.model)),
      switchMap(term => term.length > 2 ?
        this.jservices.autosuggestionsearch(term,'2') : []
      )
    )

  formatter = (x: { search_index: string }) => x.search_index + "";


  inputformatter = (x: { search_index: string, search_value: string }) => {
    this.strService = x.search_value
    console.log(this.model)
    return x.search_index ? x.search_index + "" : "";
  };
  verifyService() {
    if (this.strService == null) {
      this.newoffer.need_customservice = this.model;
      this.newoffer.need_serviceid = '';
      this.newoffer.need_subserviceid = '';

    } else {
      const x = this.strService.split("-");
      console.log("x= " + x);
      this.newoffer.need_customservice = '';
      if (x.length == 3) {
        this.newoffer.need_serviceid = x[1];
        this.newoffer.need_subserviceid = x[2];
        this.newoffer.need_customservice=this.model['search_index'];

      } else {
        this.newoffer.need_serviceid = x[1];
        this.newoffer.need_subserviceid = x[2];
        this.newoffer.need_customservice=this.model['search_index'];
      }
    }

  }
  shortlist() {
    this.jservices.addusertoshortlist(this.userId)
      .subscribe(data => {
        if(this.profileViewData['is_shortlist'])
        this.profileViewData['is_shortlist']=0;
        else
        this.profileViewData['is_shortlist']=1;
        console.log(data['message']);
      });
  }
  goBack() {
    this.location.back(); // <-- go back to previous location on cancel

    // this.router.navigate(['/dashbord']);
  }
  open(content) {
    this.modalService.open(content, { centered: true }).result.then((result) => {
    }, (reason) => {
    });
  }
  startchat() {
    this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['chats'] } }], { queryParams: { id: this.userId, name: this.profileViewData['name'], pic: this.profileViewData['profile_image'] } });

  }

  makeoffer(content2) {
    this.modalService.open(content2, { centered: true }).result.then((result) => {
    }, (reason) => {
    });
  }
  addmakeoffer() {
    this.verifyService();
    this.newoffer.offer_serviceid='8';
    this.newoffer.offer_customservice='a payment of';
    this.newoffer.user_id=this.currentuser.Id+"";
    this.newoffer.user_with=this.profileViewData['id']+"";
    this.jservices.makeoffer(this.newoffer).subscribe(data=>{
      this.modalService.dismissAll();
    })
  }
}
