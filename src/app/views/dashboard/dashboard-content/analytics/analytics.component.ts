import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { JugglrServices } from 'src/app/Services/JugglrServices';
import { User } from 'src/app/models/user';
declare var places: any;

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {  
  topservice: any;
  topactive: any;
  lat: any;
  long: any;
  jobplacesautocomplete: any;
  distance: any;
  postcode: any;
  jobcurrentlocation: any;
  type: number = 1;  
  page: number = 1;
  mapzoom: number = 18;
  
  constructor(private router: Router, private jservices: JugglrServices, private currentuser: User) { }
  radiusArray: any[] = [{id:'5',name:'5'},{id:'10',name:'10'},{id:'15',name:'15'},{id:'20',name:'20'},{id:'25',name:'25'},{id:'30',name:'30'},{id:'40',name:'40'},{id:'50',name:'50'}];
  filtertext:string='1';
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  
  ngOnInit() {
    const profile = JSON.parse(localStorage.getItem('currentUser'));
    this.currentuser.myconstructor(profile);
    this.lat = this.currentuser.latitude;
    this.long = this.currentuser.longitude;
    this.distance = 10;
    this.jobcurrentlocation = this.currentuser.address;
    this.postcode = this.currentuser.postcode;
    this.topactiveuser();
    this.topservices();
    this.jobplacesautocomplete = places({
      container: window.document.querySelector('#address-input'),
      countries: 'au'
    });

    this.jobplacesautocomplete.setVal(this.currentuser.address);
    this.jobplacesautocomplete.on('change', e => this.changeAddressToFindProfessional(e.suggestion));

   try {
      const SearchSelection = JSON.parse(localStorage.getItem('SearchSelection'));
      if(SearchSelection) {
         localStorage.removeItem('SearchSelection');
         this.jobcurrentlocation = SearchSelection["location"];
         this.distance=  SearchSelection["radius"];
      }
   } 
   catch (error) { }   
   this.loadMap();
  }

  changeAddressToFindProfessional(searchData) {
    this.jobcurrentlocation = (searchData.value);
    this.lat = searchData.latlng.lat;
    this.long = searchData.latlng.lng;
    this.postcode = '';
    if(typeof searchData.postcodes != 'undefined')
    {
      this.postcode = searchData.postcodes;
    }
    this.onchangeradius(this.distance);
  }

  loadMap(){
    this.setgooglezoom();
    var mapProp = {
      center: new google.maps.LatLng(this.lat, this.long),
      zoom: this.mapzoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    this.jservices.getmapviewdataforanalytics("" + this.lat, "" + this.long, ''+this.distance)
      .subscribe(data => {
        if (data['status'] == 1 && data['data']['mapdata'].length > 0) {
          let tmpcustomdata = [];

          for (var i = 0; i < data['data']['mapdata'].length; i++) {
            let tmplatlongarray = {};
            tmplatlongarray['latitude'] = data['data']['mapdata'][i]['latitude'];
            tmplatlongarray['longitude'] = data['data']['mapdata'][i]['longitude'];
            tmplatlongarray['totaluser'] = data['data']['mapdata'][i]['users'].length;
            tmplatlongarray['userids'] = '';
            let userlists = data['data']['mapdata'][i]['users'];
            let tmpuserlists = [];
            for (var k = 0; k < userlists.length; k++) {
              var userexists = this.inusersarray(tmpuserlists, userlists[k]['user_id']);
              if(userexists == false && k == 0) {
                tmplatlongarray['userids'] = userlists[k]['user_id'];
              } else if(userexists == false) {
                tmplatlongarray['userids'] = tmplatlongarray['userids'] + "," + userlists[k]['user_id'];
              }

              tmpuserlists.push(userlists[k]['user_id']);
            }

            let result = this.inlatlongarray(tmpcustomdata, tmplatlongarray['latitude'], tmplatlongarray['longitude']);
            if(result == true) {
              tmplatlongarray['totaluser'] += 1;
            }
            else {
              tmpcustomdata.push(tmplatlongarray);
            }
          }
          this.setgooglemarker(tmpcustomdata);
        }
      }
    );
  }

  private inusersarray(array, userid) {
    for(var i=0;i<array.length;i++) {
      return (array[i] == userid);
    }
    return false;
  }

  private inlatlongarray(array, lat, lng) {
    for(var i=0;i<array.length;i++) {
      return (array[i]['latitude'] === lat && array[i]['longitude'] === lng);
    }
    return false;
  }

  private setgooglezoom() {

    if(this.distance < 10)
    {
      this.mapzoom = 16;
    }
    else if(this.distance < 15)
    {
      this.mapzoom = 14;
    }
    else if(this.distance < 20)
    {
      this.mapzoom = 12;
    }
    else if(this.distance < 25)
    {
      this.mapzoom = 11;
    }
    else if(this.distance < 30)
    {
      this.mapzoom = 10;
    }
    else if(this.distance < 35)
    {
      this.mapzoom = 9;
    }
    else if(this.distance < 45)
    {
      this.mapzoom = 8;
    }
    else if(this.distance < 55)
    {
      this.mapzoom = 7;
    }
  }

  private setgooglemarker(customdata) {
    let tmprouter = this.router;
    for (var i = 0; i < customdata.length; i++) {
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(customdata[i].latitude, customdata[i].longitude),
        label: {
          text: customdata[i].totaluser + "",
          color: "white"
        },
        map: this.map
      });

      google.maps.event.addListener(marker, "mouseover", function(evt) {
        let label = this.getLabel();
        label.color = "black";
        this.setLabel(label);
      });
      google.maps.event.addListener(marker, "mouseout", function(evt) {
        let label = this.getLabel();
        label.color = "white";
        this.setLabel(label);
      });

      let tmpuserid = customdata[i]['userids'];
      let tmplat = customdata[i].latitude;
      let tmplng = customdata[i].longitude;
      let thisPage =  this;
      marker.addListener('click', function() {
         thisPage.saveSearch();
         tmprouter.navigate(['/dashbord', { outlets: { dashboardContent: ['userprofilelist'] } }], { queryParams: { lat:tmplat, lng:tmplng, id:tmpuserid } });
        //this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: data['data'][a[0]._index].user_id } });
      });
    }
  }
   saveSearch() {
      var SearchSelection = { location: this.jobcurrentlocation, radius : this.distance}
      console.log("SearchSelection : ",SearchSelection);
      localStorage.setItem('SearchSelection',JSON.stringify(SearchSelection));
   }
  onchangeradius(radius) {
    this.distance = radius;    
    this.topactiveuser();
    this.topservices();
    this.loadMap();
  }

  topactiveuser() {
    this.jservices.gettopactiveuser(this.lat, this.long, this.distance)
      .subscribe(data => {
        console.log(data)
        var labels = []
        var datatopactive = []
        var ids = [];
        for (var i = 0; i < data['data'].length && i < 10; i++) {
          var element = data['data'][i]
          labels.push(element.firstname)
          datatopactive.push(element.jobcount)
          ids.push(element.user_id)
        }
        this.topactive = {
          'type': 'horizontalBar',
          'data': {
            labels: labels,
            datasets: [
              {
                label: '# of gigs',
                data: datatopactive,
                x: ids,
                backgroundColor: [
                  '#bb624a',
                  '#c06e58',
                  '#c57a66',
                  '#cb8775',
                  '#d09383',
                  '#d6a091',
                  '#dbac9f',
                  '#e0b8ae',
                  '#e6c5bc',
                  '#ebd1ca',
                  '#f0ddd8',
                  '#f6eae6',

                ],
                borderColor: [
                  '#bb624a',
                  '#c06e58',
                  '#c57a66',
                  '#cb8775',
                  '#d09383',
                  '#d6a091',
                  '#dbac9f',
                  '#e0b8ae',
                  '#e6c5bc',
                  '#ebd1ca',
                  '#f0ddd8',
                  '#f6eae6',
                ],
              }
            ]
          },
          'options': {
            legend: false,
            responsive: true,
            maintainAspectRatio: true,
            scales: {
              yAxes: [{
                ticks: {
                  suggestedMin: 0,
                }
              }],
              xAxes: [{
                ticks: {
                  suggestedMin: 0,
                }
              }]
            },
            events: ["mousemove", "mouseout", "click", "touchstart", "touchmove", "touchend"],
            onClick: (index, a) => {
              if (-1 < a[0]._index) {
                  this.saveSearch();
                  this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: data['data'][a[0]._index].user_id } });
              }
            },
          }
        }
      })
  }
  topservices() {
   this.jservices.gettopservicesoffered(this.lat, this.long, this.distance)
      .subscribe(data => {
         console.log(data);
        var labels = []
        var datatopservice = []
        for (var i = 0; i < data['data'].length && i < 10; i++) {
          var element = data['data'][i]
          labels.push(element.label)
          datatopservice.push(element.count_services)
        };
        this.topservice = {
          'type': 'horizontalBar',
          'data': {
            labels: labels,
            datasets: [
              {
                label: '# of professionals',
                data: datatopservice,
                backgroundColor: [
                  '#5982ae',
                  '#668cb4',
                  '#7396bb',
                  '#80a0c1',
                  '#8daac7',
                  '#9ab4ce',
                  '#a8bdd4',
                  '#b5c7db',
                  '#c2d1e1',
                  '#cfdbe8',
                ],
                borderColor: [
                  '#1c2b3a',
                  '#233447',
                  '#293e54',
                  '#2f4861',
                  '#36516e',
                  '#3c5b7c',
                  '#436589',
                  '#496f96',
                  '#5078a3',
                  '#5078a3',
                ],
              }
            ]
          },
          'options': {
            legend: false,
            responsive: true,
            maintainAspectRatio: true,
            scales: {
              yAxes: [{
                ticks: {
                  suggestedMin: 0,
                }
              }],
              xAxes: [{
                ticks: {
                  suggestedMin: 0,
                }
              }]
            },
            onClick: (index, a) => {
              if (-1 < a[0]._index) {
                this.saveSearch();
                this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['pws'] } }], { queryParams: { id: data['data'][a[0]._index].subservice_id,searchtext:data['data'][a[0]._index].label } });
              }
            },
          }
        }
      })
  }

  lookfor(indx){
    console.log(indx)
    this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['pws'] } }], { queryParams: { id: indx.id,searchtext:indx.searchtext } });
  }
}
