import { Component, OnInit, HostListener } from '@angular/core';
import { JugglrServices } from '../../../../Services/JugglrServices';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { User } from '../../../../models/user';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { debounceTime, switchMap, tap } from 'rxjs/operators';

@Component({
   selector: 'app-network',
   templateUrl: './network.component.html',
   styleUrls: ['./network.component.css']
})
export class NetworkComponent implements OnInit {
   jobs = [];
   bpage: number = 1;
   message: string = '';
   page: number = 1;
   type: number = 1;
   filter: any[] = [{ id: '1', name: 'all jobs' }, { id: '2', name: 'my jobs' }, { id: '3', name: 'saved jobs' }, { id: '4', name: 'assigned jobs' }];
   filtertext: string = '1';
   textsearch: string;
   usersprofiles = [];
   businessarround = [];
   valuesearch = [{ id: 56, searchtext: "Sales & Marketing" },
   { id: 43, searchtext: "ADMIN SUPPORT" },
   { id: 55, searchtext: "Web design" },
   { id: 69, searchtext: "Customer service" },
   { id: 58, searchtext: "Photography" },
   { id: 52, searchtext: "Website & app testing" },
   { id: 54, searchtext: "Graphic design" },
   { id: 49, searchtext: "Social media marketing" },
   { id: 42, searchtext: "Bookkeeping & accounting" },
   { id: 50, searchtext: "Content creation & comms" },
   ];
   listofids: any;
   pagegenumber = 1;
   users: any;
   totalpages = 0;
   searchsubserviceid: string;
   searchtext: string;
   uprofessionals: any;
   ubusinesses: any;
   showtype: number;
   searchVal = "";
   searchbox: any;
   blnSearched:boolean=false;
   constructor(private jservices: JugglrServices, private router: Router, private route: ActivatedRoute, private currentuser: User) { }

   ngOnInit() {
      var k = 1
      this.users = [];
      this.ubusinesses = [];
      this.uprofessionals = [];
      this.showtype = 1;
      this.getNetworkData();

   }

   getNetworkData() {
      this.jservices.getuserconnection(this.pagegenumber + "", this.searchVal).subscribe(data => {
         console.log(data)
         this.totalpages = data['data']['totalpages']
         for (let i = 0; i < data['data']['connections'].length; i++) {
            var usersp = data['data']['connections'][i];
            if (usersp.type == 2) {
               this.ubusinesses.push(usersp);
            }
            else if (usersp.type == 1) {
               this.uprofessionals.push(usersp);
            }

         }
         //this.getnext()
      })
   }
   /*
   @HostListener("window:scroll", ['$event'])
   onScroll($event:Event): void {
      //console.log("Scrolling" , $event);
      //if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      //   console.log("at the bottom of the page");
      //   this.getnext();      
      //}
      if ($(document).height() - $(this).height() == $(this).scrollTop()) {
         console.log("End")
      }

   }
   */
   getDocumentHeight() {
      var body = document.body,
         html = document.documentElement;

      return Math.max(body.scrollHeight, body.offsetHeight,
         html.clientHeight, html.scrollHeight, html.offsetHeight);
   }
   onPageScroll() {
      console.log("Page Scrolled");
      this.getnext();
   }

   getnext() {
      console.log("this.pagegenumber : " + this.pagegenumber + " this.totalpages : " + this.totalpages)
      if (this.pagegenumber < this.totalpages) {
         this.pagegenumber++;
         console.log("this.pagegenumber : " + this.pagegenumber + " this.totalpages : " + this.totalpages)
         this.jservices.getuserconnection(this.pagegenumber + "", this.searchVal).subscribe(data => {
            console.log(data)
            this.totalpages = data['data']['totalpages']
            for (let i = 0; i < data['data']['connections'].length; i++) {
               var usersp = data['data']['connections'][i];
               if (usersp.type == 2) {
                  this.ubusinesses.push(usersp);
               }
               else if (usersp.type == 1) {
                  this.uprofessionals.push(usersp);
               }
            }
         })
      }
   }
   /*
   getnext(){
      this.pagegenumber++;
      if (this.pagegenumber<this.totalpages){
        this.jservices.getlistuserswithskill(''+this.listofids,this.searchtext,this.pagegenumber+"").subscribe(data=>{
          console.log(data)
          this.totalpages=data['data']['totalpages']
          for(let i=0;i<data['data']['connections'].length;i++){
            var usersp=data['data']['connections'][i];
            if(usersp.type==2)
            this.ubusinesses.push(usersp);
            else if(usersp.type==1)
            this.uprofessionals.push(usersp);        
        }
          this.getnext()
          console.log(this.users)
  
        })
      }
    }
    */


   shortlist(usr: any, i: number) {
      this.jservices.addusertoshortlist(usr.user_id)
         .subscribe(data => {
            if (this.users[i].is_shortlist)
               this.users[i].is_shortlist = 0;
            else
               this.users[i].is_shortlist = 1;
            console.log(data['message']);
         });
   }
   goUserDetail(userId) {
      this.router.navigate(['/dashbord', { outlets: { dashboardContent: ['profileview'] } }], { queryParams: { id: userId } });
   }

   professionals() {
      window.document.getElementById('a1').classList.add("active");
      window.document.getElementById('a2').classList.remove("active");
      this.showtype = 1;


   }
   businesses() {
      window.document.getElementById('a2').classList.add("active");
      window.document.getElementById('a1').classList.remove("active");
      this.showtype = 2;

   }
   addgig() {
      this.router.navigate(['/addjob']);

   }
   hiretalent() {
      this.router.navigate(['/dashbord']);

   }
   search = (text$: Observable<string>) =>
   text$.pipe(
      debounceTime(200),
      tap(() => {
         //console.log(this.searchbox)
         //this.searchVal = this.searchbox;
         this.searchVal = (<HTMLInputElement>window.document.getElementsByName('textsearch')[0]).value;
         if(this.searchbox != undefined && this.searchVal == this.searchbox.search_index) { 
            this.searchVal = "";
         }
         console.log("search 1 tap ::" , this.searchbox);
      }),
      switchMap(term => term.length > 2 ?
         this.jservices.autosuggestionsearch(term,'1') : []
      )
   )

   formatter = (x: { search_index: string }) => x.search_index + "";

   inputformatter = (x: { search_index: string, search_value: string }) => {
      //x.search_value
      console.log(this.searchbox)
      return x.search_index ? x.search_index + "" : "";
   };

   selectedItem(item) {
      //this.clickedItem=item.item;
      if (item) {
         console.log("searchVal", item.item.search_index);
         this.searchVal = item.item.search_index;
         this.searchbox = item.item;
         this.changesearch();
      }
   }
   clearSearch() {
      this.searchVal = "";
      this.searchbox = undefined;
      this.changesearch();
   }
   
   changesearch() {
      if(this.searchbox == undefined || this.searchVal != this.searchbox.search_index) {
         if(this.searchVal != "") {
            return;
         }
      }
      this.ubusinesses = [];
      this.uprofessionals = [];
      this.pagegenumber = 1;
      if(this.searchVal != "") {
         this.blnSearched = true;   
      }
      else {
         this.blnSearched = false;   
      }         
      this.getNetworkData();
   }
}
