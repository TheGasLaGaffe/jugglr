import { Component } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';


@Component({
  selector: 'app-modal-config',
  templateUrl: './modal-config.component.html',
  styleUrls: ['./modal-config.component.css']
})
export class ModalConfigComponent {
  constructor(config: NgbModalConfig, private modalService: NgbModal,private router: Router) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
  }

  open(content) {
    this.modalService.open(content, { centered: true });
  }

  goBusinessLogin(){
    const from=localStorage.getItem('from');

    if (from){
      localStorage.removeItem('from');
      localStorage.setItem('typeUser','2');
      this.router.navigate(['addjob']);
    }else{
      this.router.navigate(['/signinbusiness']);
    }

    this.modalService.dismissAll();
  }

  goMumLogin(){
    const from=localStorage.getItem('from');

    if (from){
      localStorage.removeItem('from');
      localStorage.setItem('typeUser','1');
      this.router.navigate(['signinmum']);
    }else{
      this.router.navigate(['/signinmum']);
    }

    this.modalService.dismissAll();
  }
  d(s){
    this.modalService.dismissAll()
  }
}
