import {
  Component
} from '@angular/core';

import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Router } from '@angular/router';
import { JugglrServices } from '../../Services/JugglrServices';


declare var stripe: any;
declare var elements: any;


@Component({
  selector: 'app-bank-account-modal',
  templateUrl: './bank-account-modal.component.html',
  styleUrls: ['./bank-account-modal.component.css']
})

export class BankAccountModalComponent {

  routing_number: string = '';
  account_number: string = '';

  constructor(config: NgbModalConfig, private modalService: NgbModal, private jservices: JugglrServices, private router: Router) {
  }

  open(content) {
    this.modalService.open(content, { centered: true });
  }

  async onSubmit() {
    const { token, error } = await stripe.createToken('bank_account', {
      country: 'AU',
      currency: 'aud',
      routing_number: this.routing_number,
      account_number: this.account_number,
      account_holder_name: 'Jenny Rosen',
      account_holder_type: 'individual',
    });


    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      this.jservices.updateccdetails('', '', token['id'], '')
        .subscribe(data => {
          console.log(data);
          if (data['status'] == 1) {
            var profile = JSON.parse(localStorage.getItem('currentUser'));
            profile['stripe_acct_id'] = data['data']['stripe_acct_id'];
            localStorage.setItem('currentUser', JSON.stringify(profile));
            this.modalService.dismissAll();

          }
        });
    }
  }
  d(s){
    this.modalService.dismissAll();
  }
}

