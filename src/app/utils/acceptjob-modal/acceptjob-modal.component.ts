import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {JugglrServices} from '../../Services/JugglrServices';
import {Router} from '@angular/router';

@Component({
  selector: 'app-acceptjob-modal',
  templateUrl: './acceptjob-modal.component.html',
  styleUrls: ['./acceptjob-modal.component.css']
})
export class AcceptjobModalComponent implements OnInit {
date:string='';
note:string='';
  constructor(private modalService: NgbModal,private jservices: JugglrServices,private router: Router) { }

  ngOnInit() {
  }
  open(content) {
    this.modalService.open(content, { centered: true });
  }
  d(s){
    this.modalService.dismissAll();
  }
}
