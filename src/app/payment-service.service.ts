import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';


@Injectable({
   providedIn: 'root'
})
export class PaymentServiceService {

   userId: string;

   constructor(private db: AngularFireDatabase, private http: HttpClient) {

   }
   processcard(token: any, amount: number) {
      const payment = { token, amount }
      return payment;
   }
   getAllPlans() {
      const httpOptions = {
         headers: new HttpHeaders()
            .set('Authorization', 'Bearer ' + environment.stripeSecretKey)
      };
      return this.http.get('https://api.stripe.com/v1/plans', httpOptions);
   }

   processPayment(token: any, amount: number, planID:string, callback:any) {
      const payment = { token, amount }

      const body = new HttpParams()
         .set('email', payment.token.email)
         .set('source', payment.token.id);


      const httpOptions = {
         headers: new HttpHeaders()
            .set('url', 'https://api.stripe.com/v1/customers')
            .set('method', 'POST')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .set('Authorization', 'Bearer ' + environment.stripeSecretKey)
      };

      console.log('updateccdetails');
      console.log(body);
      console.log(httpOptions);
      (this.http.post('https://api.stripe.com/v1/customers', body, httpOptions)).subscribe(data => {
         console.log(data)

         const cust = data['id'];

         // Remove this condition once set dynamic plan id from business-premium.component 
         if(planID == "") {
            planID = "plan_EnoGay4gNgfJgC";
         }

         console.log(cust)
         const body = new HttpParams()
            .set('customer', cust)
            .set('items[0][plan]', planID);


         const httpOptions = {
            headers: new HttpHeaders()
               .set('url', 'https://api.stripe.com/v1/subscriptions')
               .set('method', 'POST')
               .set('Content-Type', 'application/x-www-form-urlencoded')
               .set('Authorization', 'Bearer ' + environment.stripeSecretKey)
         };

         console.log('updateccdetails');
         console.log(body);
         console.log(httpOptions);
         this.http.post('https://api.stripe.com/v1/subscriptions', body, httpOptions).subscribe(data => {
            console.log(data)
            if(callback) {
               callback();
            }
         })
      });


      //return this.db.list(`/payments`).push(payment)
   }
}
