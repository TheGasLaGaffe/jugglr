import { Injectable } from '@angular/core';

@Injectable()
export class User { 
    Id: string = '';
    type: string = '';
    firstname : string = '';
    lastname : string = '';
    profile_image_thumb : string = '';
    dob : string = '';
    gender : string = '';
    about_me : string = '';
    suburb : string = '';
    city : string = '';
    state: string = '';
    country: string = '';
    facebook_id: string = '';
    facebook_token : string = '';
    email : string = '';
    postcode : string = '';
    latitude : string = '';
    longitude : string = '';
    user_token: string = '';
    device_token : string = '';
    device_type : string = '';
    service_offered  : string = '';
    service_needed: string = '';
    password : string = '';
    password_token : string = '';
    address : string = '';
    webaddress : string = '';
    phone  : string = '';
    contactus : string = '';
    star_rating:number=0;
  stripe_acct_id:string='';
  stripe_cust_id:string='';

  myconstructor(json:JSON){
    this.Id=json['id']+"";
    this.type=json['type'];
    this.firstname=json['firstname'];
    this.lastname=json['lastname'];
    this.profile_image_thumb=json['profile_image_thumb'];
    this.about_me=json['about_me'];
    this.suburb=json['suburb'];
    this.city=json['city'];
    this.state=json['state'];
    this.country=json['country'];
    this.facebook_id=json['facebook_id'];
    this.facebook_token=json['facebook_token'];
    this.email=json['email'];
    this.postcode=json['postcode']+"";
    this.latitude=json['latitude'];
    this.longitude=json['longitude'];
    this.user_token=json['user_token']+"";
    this.device_type=json['device_type'];
    this.service_offered=json['service_offered'];
    this.service_needed=json['service_needed'];
    this.password=json['password'];
    this.address=json['address'];
    this.webaddress=json['webaddress'];
    this.phone=json['phone'];
    this.contactus=json['contactus'];
    this.star_rating=json['star_rating'];
    this.stripe_acct_id=json['stripe_acct_id']+"";
    this.stripe_cust_id=json['stripe_cust_id']+"";
  }
  toJSONdata(json:JSON){
    json['id']=this.Id+""
    json['type']=this.type
    json['firstname']=this.firstname
    json['lastname']=this.lastname
    json['profile_image_thumb']=this.profile_image_thumb;
    json['about_me']=this.about_me;
    json['suburb']=this.suburb;
    json['city']=this.city;
    json['state']=this.state;
    json['country']=this.country;
    json['facebook_id']=this.facebook_id
    json['facebook_token']=this.facebook_token
    json['email']=this.email;
    json['postcode']=this.postcode+"";
    json['latitude']=this.latitude;
    json['longitude']=this.longitude;
    json['user_token']=this.user_token;
    json['device_type']=this.device_type;
    json['service_offered']=this.service_offered;
    json['service_needed']=this.service_needed;
    json['password']=this.password;
    json['address']=this.address;
    json['webaddress']=this.webaddress;
    json['phone']=this.phone;
    json['contactus']=this.contactus;
    json['star_rating']=this.star_rating;
    json['stripe_acct_id']=this.stripe_acct_id;
    json['stripe_cust_id']=this.stripe_cust_id;
  }
}
