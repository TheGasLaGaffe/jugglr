import { Injectable } from '@angular/core';

@Injectable()
export class Job { 
    Id: string = '';
    Type: string = '0';
    Service_id: string = '';
    Subservice_id: string = ''; 
    Subservicetag_id: string = ''; 
    Customservice: string = ''; 
    Datetime: string = ''; 
    Location: string = '';
    Amount: string = '';
    Note: string = '';
    Promotion_id: string = '';
    Discount: string = '';
    Postcode: string = '';
    User_id: string = '';
    User_token: string = '';
    Device_type: string = '';
    user_with: Number= 0;
}
