import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { type } from 'os';

@Injectable()
export class S3UploadFileService {
   FOLDER = ''; //'jugglrmobileimage/'
   constructor() { }
   uploadfile(file, successCallback, errorCallback) {
      const bucket = new S3(environment.s3Params);
      const fileData = {
         Bucket: environment.s3Bucket,
          Key: this.FOLDER + file.name,
          Body: file,
          ACL: 'public-read',
          ContentType: file.type
      };
      bucket.upload(fileData, function (err, data) {
         if (err) {
            console.log('There was an error uploading your file: ', err);
            if(typeof (errorCallback) === "function") {
               errorCallback(err);
            }
         }
         else {
            console.log('Successfully uploaded file.', data);
            if(typeof (successCallback) === "function") {
               successCallback(data);
            }
         }
      });
   }
}    