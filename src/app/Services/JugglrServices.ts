import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Job } from '../models/job';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user';
import {AuthService} from 'angular-6-social-login';
import { environment } from '../../environments/environment';
import { Offer } from '../models/offer';


@Injectable()
export class JugglrServices {
  private Xapi: string;
  private BASEURL: string;
  private allnotificationdata = new BehaviorSubject("");
  currentnotificationdata = this.allnotificationdata.asObservable();
  constructor(private http: HttpClient,private currentuser:User) {

    this.Xapi = environment.Xapi;
    this.BASEURL = environment.server;

    //this.BASEURL = 'http://192.168.1.6/html-ws';
  }

  getAllServices() {
    const body = new HttpParams().set('type', 'services');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/getservices.json', body, httpOptions);

  }
  getAllServicesWithSubService() {
   const body = new HttpParams().set('type', '');

   const httpOptions = {
     headers: new HttpHeaders({
       'Xapi': this.Xapi
     })
   };

   return this.http.post(this.BASEURL+'/Apiwebv5s/getservices.json', body, httpOptions);

 }
  listjob() {
    const body = new HttpParams().set('type', '0');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/getservices.json', body, httpOptions);

  }

  addJob(job: Job) {
    this.getcurrentUser();
    const body = new HttpParams()
      .set('type', job.Type)
      .set('service_id', job.Service_id)
      .set('subservice_id', job.Subservice_id)
      .set('subservicetag_id', job.Subservicetag_id)
      .set('customservice', job.Customservice)
      .set('datetime', job.Datetime)
      .set('location', job.Location)
      .set('amount', job.Amount)
      .set('note', job.Note)
      .set('promotion_id', job.Promotion_id)
      .set('discount', job.Discount)
      .set('postcode', job.Postcode);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': this.currentuser.Id,
        'Usertoken': this.currentuser.user_token,
        'ePlatform': this.currentuser.device_type,
      })
    };
    console.log(body);
    console.log(httpOptions.headers);
    return this.http.post(this.BASEURL+'/Apiwebv5s/addJob.json', body, httpOptions);

  }
  sendmessagenotification(id: string) {
    this.getcurrentUser();
    const body = new HttpParams()
      .set('user_id', this.currentuser.Id)
      .set('user_with', id)
      ;

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': this.currentuser.Id,
        'Usertoken': this.currentuser.user_token,
        'ePlatform': this.currentuser.device_type,
      })
    };
    console.log(body);
    console.log(httpOptions.headers);
    return this.http.post(this.BASEURL+'/Apiwebv5s/getMessageNotification.json', body, httpOptions);

  }

  listJob(type, filter, sort, search_text, search_value, search_distance, page_no, latitude, longitude, user_id, usertoken, device_type) {
    const body = new HttpParams()
      .set('type', type)
      .set('filter', filter)
      .set('sort', sort)
      .set('search_text', search_text)
      .set('search_value', search_value)
      .set('search_distance', search_distance)
      .set('page_no', page_no)
      .set('latitude', latitude)
      .set('longitude', longitude);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user_id,
        'Usertoken': usertoken,
        'ePlatform': device_type,
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/listJob.json', body, httpOptions);

  }

  

  getJobDetail(job_id, latitude, longitude, user_id, usertoken, device_type) {
    const body = new HttpParams()
      .set('job_id', job_id)
      .set('latitude', latitude)
      .set('longitude', longitude);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user_id,
        'Usertoken': usertoken,
        'ePlatform': device_type,
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/getJobDetail.json', body, httpOptions);

  }

  listJobComment(job_id, user_id, usertoken, device_type,npage) {
    const body = new HttpParams()
      .set('job_id', job_id)
      .set('page_no', npage+'');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user_id,
        'Usertoken': usertoken,
        'ePlatform': device_type,
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/listJobComment.json', body, httpOptions);

  }

  addJobComment(job_id, comment, user_id, usertoken, device_type) {
    const body = new HttpParams()
      .set('job_id', job_id)
      .set('comment', comment);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user_id,
        'Usertoken': usertoken,
        'ePlatform': device_type,
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/addJobComment.json', body, httpOptions);

  }
  registration(user:User) {
    const body = new HttpParams()
      .set('facebook_id', user.facebook_id)
      .set('facebook_token', user.facebook_token)
      .set('device_token', '')
      .set('firstname', user.firstname)
      .set('lastname', user.lastname)
      .set('profile_image_thumb', user.profile_image_thumb)
      .set('email', user.email);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': '89c83fcJKS5514OD59283fgf15150c6hhs444ishDF',
        'ePlatform': 'ios',
      })
    };
    console.log(body);
    console.log(httpOptions.headers);
    return this.http.post(this.BASEURL+'/Apiwebv5s/registration.json', body, httpOptions);

  }
  registrationwithdetail(user:User) {
    const body = new HttpParams()
      .set('facebook_id', user.facebook_id)
      .set('facebook_token', user.facebook_token)
      .set('device_token', '')
      .set('firstname', user.firstname)
      .set('lastname', user.lastname)
      .set('profile_image_thumb', user.profile_image_thumb)
      .set('email', user.email)
      .set('postcode',user.postcode)
      .set('lat',user.latitude)
      .set('long',user.longitude)
      .set('ip','03.3.3.0')
      .set('gender','female')
      .set('service_needed', user.service_needed != undefined ? JSON.stringify(user.service_needed): '[]')
      .set('service_offered', user.service_offered != undefined ? JSON.stringify(user.service_offered): '[]');
      

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'ePlatform': 'ios',
      })
    };
    console.log(body);
    console.log(httpOptions.headers);
    return this.http.post(this.BASEURL+'/Apiwebv5s/registration.json', body, httpOptions);

  }


  getPendingNotification(user_id, usertoken, device_type,pageno) {
    const body = new HttpParams()
      .set('page_no', pageno+'');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user_id,
        'Usertoken': usertoken,
        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/getPendingNotification.json', body, httpOptions);
  }

  getserviceproviderdetail(id_user) {
    const body = new HttpParams()
      .set('user_id', id_user + '')
      .set('current_user', '4472');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': '4472',
        'Usertoken': '929239a16bb29d01322e245af7057ab21492520269',
        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/getserviceproviderdetail.json', body, httpOptions);
  }

  autosuggestionsearch(term, type): Observable<any> {
    const body = new HttpParams()
      .set('search_text', term)
      .set('type', type); 
      /*
      IF type == 1 :: NAME/SUBRUB/POSTCODE/SERVICE/SUBSERVICES
      IF type == 2 :: SERVICE/SUBSERVICE
      */
      
    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,

        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/autosuggestionsearch.json', body, httpOptions)
      .pipe(

        map((response: Response) => response['data']['search_result'])
      );
  }

  businessregistration(user: User) {
    const body = new HttpParams()
      .set('email', user.email)
      .set('password', user.password)
      .set('firstname', user.firstname)
      .set('postcode', user.postcode)
      .set('city', user.city!=null? user.city:'' )
      .set('country', user.country!=null? user.country:'')
      .set('suburb', user.suburb!=null? user.suburb:'')
      .set('state', user.state!=null? user.state:'')
      .set('address', user.address!=null? user.address:'')
      .set('lat', user.latitude)
      .set('long', user.longitude)
      .set('phone', user.phone!=null? user.phone:'')
      .set('webaddress', user.webaddress!=null? user.webaddress:'')
      .set('contactus', user.contactus!=null? user.contactus:'')
      .set('about_me',user.about_me!=null? user.about_me:'');
      //.set('profile_image_thumb',user.profile_image_thumb!=''?user.profile_image_thumb:'');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/businessregistrationforelio.json', body, httpOptions);
  }
  updatebusinessuser(user: User) {
    const body = new HttpParams()
      .set('user_id',user.Id)
      .set('email', user.email)
      .set('firstname', user.firstname)
      .set('postcode', user.postcode)
      .set('city', user.city!=null? user.city:'' )
      .set('country', user.country!=null? user.country:'')
      .set('suburb', user.suburb!=null? user.suburb:'')
      .set('state', user.state!=null? user.state:'')
      .set('address', user.address!=null? user.address:'')
      .set('lat', user.latitude)
      .set('long', user.longitude)
      .set('phone', user.phone!=null? user.phone:'')
      .set('webaddress', user.webaddress!=null? user.webaddress:'')
      .set('contactus', user.contactus!=null? user.contactus:'')
      .set('about_me',user.about_me!=null? user.about_me:'')
      .set('Usertoken', user.user_token)

      .set('profile_image_thumb',user.profile_image_thumb!=''?user.profile_image_thumb:'');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user.Id,
        'Usertoken': user.user_token,
        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/updatebusinessuser.json', body, httpOptions);
  }
  getcurrentUser(){
    let profile=JSON.parse(localStorage.getItem('currentUser'));
    console.log(profile)
    this.currentuser.about_me=profile['about_me'];
    this.currentuser.Id=profile['id']+'';
    this.currentuser.facebook_id=profile['facebook_id'];
    this.currentuser.device_token=profile['device_token'];
    this.currentuser.user_token=profile['user_token'];
    this.currentuser.device_type=profile['device_type'];
  }
  businessLogin(user:User){
    const body = new HttpParams()
      .set('email', user.email)
      .set('password', user.password)
      .set('device_token', '');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': '89c83fcJKS5514OD59283fgf15150c6hhs444ishDF',
        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/login.json', body, httpOptions);
  }

  signupmum(user:User) {
    const body = new HttpParams()
      .set('facebook_id', user.facebook_id)
      .set('facebook_token', user.facebook_token)
      .set('device_token', '')
      .set('firstname', user.firstname)
      .set('lastname', user.lastname)
      .set('profile_image_thumb',user.profile_image_thumb)
      .set('lat', user.latitude)
      .set('long', user.longitude)
      .set('ip', '')
      .set('postcode', user.postcode)
      .set('email', user.email)
      .set('gender','female');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'ePlatform': 'ios',
      })
    };
    console.log(body);
    console.log(httpOptions.headers);
    return this.http.post(this.BASEURL+'/Apiwebv5s/registration.json', body, httpOptions);

  }

  savejob(job_id,user:User,type) {
    const body = new HttpParams()
      .set('job_id', job_id)
      .set('type', type);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user.Id,
        'Usertoken': user.user_token,
        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/saveJob.json', body, httpOptions);
  }

  getUserDetail(user_id,user:User) {
    const body = new HttpParams()
      .set('user_id', user_id)
      .set('current_user', user.Id);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user.Id,
        'Usertoken': user.user_token,
        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/getserviceproviderdetail.json', body, httpOptions);
  }

  getUserfullDetail(user_id,user:User) {
    const body = new HttpParams()
      .set('user_id', user_id)
      .set('current_user', user.Id);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': user.Id,
        'Usertoken': user.user_token,
        'ePlatform': 'ios',
      })
    };

    return this.http.post(this.BASEURL+'/Apiwebv5s/getserviceproviderfulldetail.json', body, httpOptions);
  }

  addusertoshortlist(userwith: string) {
    this.getcurrentUser();
    const body = new HttpParams()
      .set('user_id', this.currentuser.Id)
      .set('user_with', userwith);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': this.currentuser.Id,
        'Usertoken': this.currentuser.user_token,
        'ePlatform': 'ios',
      })
    };

    console.log('/////////////////addusertoshortlist//////////////////');
    console.log(body);
    console.log(httpOptions);

    return this.http.post(this.BASEURL + '/Apiwebv5s/addusertoshortlist.json', body, httpOptions);
  }
  updateccdetails(stripe_card_token: string, stripe_cust_id: string, stripe_bank_token: string, stripe_acct_id: string) {
    const body = new HttpParams()
      .set('stripe_bank_token', stripe_bank_token)
      .set('stripe_card_token', stripe_card_token)
      .set('stripe_cust_id', stripe_cust_id)
      .set('stripe_acct_id', stripe_acct_id);

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': this.currentuser.Id,
        'Usertoken': this.currentuser.user_token,
        'ePlatform': 'ios',
      })
    };

    console.log('updateccdetails');
    console.log(body);
    console.log(httpOptions);

    return this.http.post(this.BASEURL + '/Apiwebv5s/updateccdetails.json', body, httpOptions);
  }
  applytojob(job_id: string, note: string, datetime: string) {
    const body = new HttpParams()
      .set('job_id', job_id)
      .set('note', note)
      .set('datetime', datetime)
      .set('location', '')
      .set('promotion_id', '')
      .set('discount', '');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': this.currentuser.Id,
        'Usertoken': this.currentuser.user_token,
        'ePlatform': 'ios',
      })
    };

    console.log('updateccdetails');
    console.log(body);
    console.log(httpOptions);

    return this.http.post(this.BASEURL + '/Apiwebv5s/makeOfferToJob.json', body, httpOptions);
  }
  getresultfromfire(id:string){
    const body = new HttpParams();
      

    const httpOptions = {
      headers: new HttpHeaders()
    };
      let link ='https://jugglrapp.firebaseio.com/service/offer/subservice/'+id+'/result.json';
    return this.http.get(link, httpOptions);
  }
  getlistusers(ids: string, tx:string) {
      const body = new HttpParams()
      .set('lat', this.currentuser.latitude)
      .set('long', this.currentuser.longitude)
      .set('page_no', '1')
      .set('search_text', ''+tx)
      .set('search_result', ""+ids)
      .set('search_distance', '0-100')
      .set('search_toggle', '4')
      .set('ending_age_range', '16')
      .set('starting_age_range', '99');

    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': this.currentuser.Id,
        'Usertoken': this.currentuser.user_token,
        'ePlatform': 'ios',
      })
    };

    console.log('updateccdetails');
    console.log(body);
    console.log(httpOptions);

    return this.http.post(this.BASEURL + '/Apiwebv5s/getlistviewdata.json', body, httpOptions);
  }
  
  getPlaceDetail(address:string) {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&region=AU&key=AIzaSyAwPVTiGnYn5uXh8JNgxx4xXGHQ42OpSiQ');
  }
  resetpassword(email:string) {
    const body = new HttpParams()
    .set('email',email );

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'ePlatform': 'ios',
    })
  };

  return this.http.post(this.BASEURL + '/Apiwebv5s/forgotpassword.json', body, httpOptions);
  }

   setpassword(password:string,confirmpassword:string, token:string) {
      const body = new HttpParams()
         .set('password', password)
         .set('confirmpassword', confirmpassword)
         .set('token', token);
      const httpOptions = {
            headers: new HttpHeaders({
            'Xapi': this.Xapi,
            'ePlatform': 'ios',
         })
      };
      return this.http.post(this.BASEURL + '/Apiwebv5s/setPassword.json', body, httpOptions);
   }


  getlistbusinessesarround(page:number) {
    const body = new HttpParams()
    .set('lat', this.currentuser.latitude)
    .set('long', this.currentuser.longitude)
    .set('page_no', ''+page)
    .set('search_text', '')
    .set('search_result', "")
    .set('search_distance', '0-10000')
    .set('search_toggle', '4')
    .set('ending_age_range', '99')
    .set('starting_age_range', '16')
    .set('discover_mode', '1')
    .set('user_id', '8146')
    .set('type', '2');

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/getlistviewdatabusiness.json', body, httpOptions);
}

getListViewDataProfessional(page:number,search_text:string) {
   const body = new HttpParams()
   .set('lat', this.currentuser.latitude)
   .set('long', this.currentuser.longitude)
   .set('page_no', ''+page)
   .set('search_text', search_text)
   .set('search_result', "")
   .set('search_distance', '0-10000')
   .set('search_toggle', '4')
   .set('ending_age_range', '99')
   .set('starting_age_range', '16')
   .set('discover_mode', '1')
   .set('user_id', '8146')
   .set('type', '2');

 const httpOptions = {
   headers: new HttpHeaders({
     'Xapi': this.Xapi,
     'user_id': this.currentuser.Id,
     'Usertoken': this.currentuser.user_token,
     'ePlatform': 'ios',
   })
 };

 console.log('updateccdetails');
 console.log(body);
 console.log(httpOptions);

 return this.http.post(this.BASEURL + '/Apiwebv5s/getlistviewdataProfessional.json', body, httpOptions);
}
  postupdategig( job:any){
    console.log(job)
    const body = new HttpParams()
    .set('job_id', job.job_id)
    .set('service_id', job.service_id)
    .set('subservice_id', job.subservice_id)
    .set('subservicetag_id', job.subservicetag_id)
    .set('customservice', job.customservice)
    .set('datetime', job.datetime)
    .set('location', job.location)
    .set('amount', job.amount)
    .set('note', job.note);

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/updateJob.json', body, httpOptions);
  }
  mytodo( type:string, page_no:string, searchtext:string){
    const body = new HttpParams()
    .set('type', type)
    .set('filter', '4')
    .set('sort', '1')
    .set('search_text', searchtext)
    //.set('page_no', '0')
    .set('page_no', page_no)
    .set('latitude', this.currentuser.latitude)
    .set('longitude', this.currentuser.longitude);

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

   //return this.http.post(this.BASEURL + '/Apiwebv5s/mytodo.json', body, httpOptions);
   return this.http.post(this.BASEURL + '/Apiwebv5s/mybooking.json', body, httpOptions);
  }
  mycompletedtodo( type:string, search_text:string){
    const body = new HttpParams()
    .set('type', type)
    .set('filter', '4')
    .set('sort', '1')
    .set('page_no', '0')
    .set('search_text', search_text)
    .set('latitude', this.currentuser.latitude)
    .set('longitude', this.currentuser.longitude);

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/mytodo.json', body, httpOptions);
  }
  assignjob( offer_id,job_id,applicant_id,action,type){
    const body = new HttpParams()
    .set('offer_id', offer_id)
    .set('job_id', job_id)
    .set('applicant_id', applicant_id)
    .set('action', action)
    .set('type', type);

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/acceptJobOffer.json', body, httpOptions);
  }
  confirmcomplete( offer_id){
    const body = new HttpParams()
    .set('offer_id', offer_id)
    .set('user_id', this.currentuser.Id)
    .set('date', (new Date().toLocaleString()))
   ;

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/setoffercomplete.json', body, httpOptions);
  }
  addtransactionfeedback( offer_id,rating){
    const body = new HttpParams()
    .set('offer_id', offer_id)
    .set('rating',rating)
    .set('user_id', this.currentuser.Id)
    .set('date', (new Date().toLocaleString()))
   ;

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/addtransactionfeedback.json', body, httpOptions);
  }

  gettransactiondetail( offer_id){
    const body = new HttpParams()
    .set('offer_id', offer_id);

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/gettransactiondetail.json', body, httpOptions);
  }
  accepttransactiondetail( offer_id,accept){
    const body = new HttpParams()
    .set('is_accepted',accept)
    .set('offer_id', offer_id)
    .set('promotion_id', '')
    .set('discount', '');

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/accepttransaction.json', body, httpOptions);
  }
  addtransaction( transaction){
    const body = new HttpParams()
    .set('is_accepted',transaction.user_id)
    .set('user_with', transaction.user_with)
    .set('need_serviceid', transaction.need_serviceid)
    .set('need_subserviceid', transaction.need_subserviceid)
    .set('need_customservice', transaction.need_customservice)
    .set('offer_serviceid', transaction.offer_serviceid)
    .set('offer_subserviceid', transaction.offer_subserviceid)
    .set('offer_customservice', transaction.offer_customservice)
    .set('need_datetime', transaction.need_datetime)
    .set('need_location', transaction.need_location)
    .set('need_note', transaction.need_note)
    .set('offer_datetime', transaction.offer_datetime)
    .set('offer_location', transaction.offer_location)
    .set('offer_note', transaction.offer_note)
    .set('amount', transaction.amount)
    .set('discount', transaction.discount)
    .set('need_subservicetagid', transaction.need_subservicetagid)
    .set('offer_subservicetagid', transaction.offer_subservicetagid)

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/addtransaction.json', body, httpOptions);
    
  }
  gettopservicesoffered( lat,long,distance){
    const body = new HttpParams()
    .set('lat',lat)
    .set('long', long)
    .set('distance', distance)
   

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/gettopservicesoffered.json', body, httpOptions);
    
  }
  gettopactiveuser( lat,long,distance){
    const body = new HttpParams()
    .set('lat',lat)
    .set('long', long)
    .set('distance', distance)
   

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/gettopactiveuser.json', body, httpOptions);
    
  }

  makeoffer(newoffer:Offer) {
    const body = new HttpParams()
    .set('user_id', newoffer.user_id)
    .set('user_with', newoffer.user_with)
    .set('need_serviceid', newoffer.need_serviceid)
    .set('need_subserviceid', newoffer.need_subserviceid)
    .set('need_customservice', newoffer.need_customservice)
    .set('offer_serviceid', newoffer.offer_serviceid)
    .set('offer_subserviceid', newoffer.offer_subserviceid)
    .set('offer_customservice', newoffer.offer_customservice)
    .set('need_datetime', newoffer.need_datetime)
    .set('need_location', newoffer.need_location)
    .set('need_note', newoffer.need_note)
    .set('offer_datetime', newoffer.offer_datetime)
    .set('offer_location', newoffer.offer_location)
    .set('offer_note', newoffer.offer_note)
    .set('amount', newoffer.amount)
    .set('discount', newoffer.discount)
    .set('need_subservicetagid', newoffer.need_subservicetagid)
    .set('offer_subservicetagid', newoffer.offer_subservicetagid)
      
    const httpOptions = {
      headers: new HttpHeaders({
        'Xapi': this.Xapi,
        'user_id': this.currentuser.Id,
        'Usertoken': this.currentuser.user_token,
        'ePlatform': 'ios',
      })
    };


    return this.http.post(this.BASEURL + '/Apiwebv5s/addtransaction.json', body, httpOptions);
  }

  getlistuserswithskill(ids: string, tx:string,page_no:string,) {
    const body = new HttpParams()
    .set('lat', this.currentuser.latitude)
    .set('long', this.currentuser.longitude)
    .set('page_no', page_no)
    .set('search_text', ''+tx)
    .set('search_result', ""+ids)
    .set('search_distance', '0-100')
    .set('search_toggle', '4')
    .set('ending_age_range', '16')
    .set('starting_age_range', '99');

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  console.log('updateccdetails');
  console.log(body);
  console.log(httpOptions);

  return this.http.post(this.BASEURL + '/Apiwebv5s/getlistviewdata.json', body, httpOptions);
}

getuserconnection(page_no:string, search_text) {
  const body = new HttpParams()
  .set('lat', this.currentuser.latitude)
  .set('long', this.currentuser.longitude)
  .set('page_no', page_no)
  .set('type', ''+"SHORTLIST")
  .set('user_id', ""+this.currentuser.Id)
  .set('search_text', search_text);
  

const httpOptions = {
  headers: new HttpHeaders({
    'Xapi': this.Xapi,
    'user_id': this.currentuser.Id,
    'Usertoken': this.currentuser.user_token,
    'ePlatform': 'ios',
  })
};

console.log('updateccdetails');
console.log(body);
console.log(httpOptions);

return this.http.post(this.BASEURL + '/Apiwebv5s/getuserconnection.json', body, httpOptions);
}
getmapviewdata(lat:string, long:string, tx:string,distance:string,) {
  const body = new HttpParams()
  .set('lat', lat)
  .set('long', long)
  //.set('page_no', page_no)
  //.set('search_text', ''+tx)
  //.set('search_text', ''+6005)
  //.set('search_result', "POSTCODE")
  .set('search_text', '')
  .set('search_result', "")
  .set('search_distance', ''+distance)
  .set('search_toggle', '4')
  .set('starting_age_range', '16')
  .set('ending_age_range', '99')
  .set('user_id', this.currentuser.Id);

const httpOptions = {
  headers: new HttpHeaders({
    'Xapi': this.Xapi,
    'user_id': this.currentuser.Id,
    'Usertoken': this.currentuser.user_token,
    'ePlatform': 'ios',
  })
};

console.log('updateccdetails');
console.log(body);
console.log(httpOptions);

return this.http.post(this.BASEURL + '/Apiwebv5s/getmapviewdata.json', body, httpOptions);
}

updatenotificationreadflag(notification_ids:string) {
  const body = new HttpParams()
  .set('notification_ids', notification_ids);

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'notification_ids': notification_ids,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };
  return this.http.post(this.BASEURL+'/Apiwebv5s/updatenotificationreadflag.json', body, httpOptions);
}

updatenotificationreaddata(notificationData) {
  this.allnotificationdata.next(notificationData);
}

getmapviewdataforanalytics(lat:string, long:string, distance:string,) {
  const body = new HttpParams()
  .set('lat', lat)
  .set('long', long)
  .set('distance', ''+distance);

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  return this.http.post(this.BASEURL + '/Apiwebv5s/getmapviewdataforanalytics.json', body, httpOptions);
}

getlistviewdataforanalytics(lat:string, long:string, user_ids:string, page_no:string) {
  const body = new HttpParams()
  .set('lat', lat)
  .set('long', long)
  .set('user_ids', user_ids)
  .set('page_no', page_no);

  const httpOptions = {
    headers: new HttpHeaders({
      'Xapi': this.Xapi,
      'user_id': this.currentuser.Id,
      'Usertoken': this.currentuser.user_token,
      'ePlatform': 'ios',
    })
  };

  return this.http.post(this.BASEURL + '/Apiwebv5s/getlistviewdataforanalytics.json', body, httpOptions);
}
/*
getAusPostcodes(postcode: string) {
 const httpOptions = {
   headers: new HttpHeaders({
      'auth-key': environment.auspostKey,
      'Access-Control-Allow-Origin':'*',
      'Access-Control-Allow-Methods':'GET, POST, PATCH, PUT, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'Origin, Content-Type, auth-key'
   })
 };
 //console.log(httpOptions);
 return this.http.get('https://digitalapi.auspost.com.au/postcode/search?q='+ postcode, httpOptions);
}
*/

getAusPostcodes(postcode: string) {
   const httpOptions = { };
   //console.log(httpOptions);
   return this.http.get(this.BASEURL + '/Apiwebv5s/searchpostcode.json?q='+ postcode, httpOptions);
}
}
